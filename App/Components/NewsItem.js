import React from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import _ from 'lodash';

import moment from 'moment';
import { Feather } from '@expo/vector-icons';
import { compareOmitState } from '../Services/Utils';
import * as WebBrowser from 'expo-web-browser';
import { Metrics, Colors, Fonts, ApplicationStyles } from '../Themes';

const NewsItem = props => {
  const { item } = props;
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.imageNews}>
          <Image style={styles.imageNews} source={{ uri: item.image }}></Image>
        </View>
        <View style={styles.content}>
          <Text style={styles.title} numberOfLines={3}>
            {item.title}
          </Text>
        </View>
        <View style={styles.desc}>
          <Text style={styles.date}>{item.date}</Text>
          <TouchableOpacity onPress={() => {
            WebBrowser.openBrowserAsync(item.url);
          }}>
            <Text style={styles.date}>Xem thêm >></Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
export default NewsItem;

const styles = StyleSheet.create({
  container: {
    // height: 100,
  },
  wrapper: {
    backgroundColor: 'white',
    borderColor: Colors.cloud,
    borderWidth: 0.1,
    borderRadius: 15,
    // shadow
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 5,
  },
  imageNews: {
    height: 100,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  content: {},
  title: {
    paddingHorizontal: 10,
    ...Fonts.style.h4,
    fontFamily: Fonts.type.semiBold,
    color: Colors.textColor.black,
  },
  desc: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  date: {
    paddingHorizontal: 10,
    ...Fonts.style.h5,
    fontFamily: Fonts.type.light,
    color: Colors.textColor.tiny,
  },
});
