import React from 'react';
import { ImageStyle } from 'react-native';
import { Icon, IconElement } from '@ui-kitten/components';

export const HeartIcon = style => <Icon {...style} name="heart" />;

export const MessageCircleIcon = style => (
  <Icon {...style} name="message-circle-outline" />
);

export const ArrowIosBackIcon = style => (
  <Icon {...style} name="arrow-ios-back" />
);
