import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import { Avatar, Button, Divider, Layout, Text } from '@ui-kitten/components';
import { ImageOverlay } from './image-overlay.component';
import * as WebBrowser from 'expo-web-browser';
import { Images, ApplicationStyles, Colors, Fonts } from '../Themes/';
import { Icon } from '@ui-kitten/components';

const HeartIcon = style => <Icon {...style} name="heart" />;

const MessageCircleIcon = style => (
  <Icon {...style} name="message-circle-outline" />
);

const NewsItem2 = ({ item, navigation }) => (
  <TouchableOpacity
    onPress={() => {
      navigation.navigate('NewsDetailScreen', {
        ...item,
      });
      // WebBrowser.openBrowserAsync(item.url);
    }}>
    <Layout style={styles.container}>
      <Layout style={styles.wrapper}>
        <ImageOverlay
          source={{ uri: item.thumbnail }}
          style={styles.headerContainer}>
          <Text
            style={styles.headerTitle}
            category="h6"
            status="control"
            numberOfLines={2}>
            {item.title}
          </Text>
        </ImageOverlay>
      </Layout>
    </Layout>
  </TouchableOpacity>
);

export default NewsItem2;

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  content: {
    flex: 1,
    paddingTop: 30,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  wrapper: {
    width: '95%',
    marginVertical: 3,
    backgroundColor: 'red',
    borderRadius: 15,
    // alignItems: 'center',
  },
  headerContainer: {
    // alignItems: 'center',
    height: 200,
    borderRadius: 15,
    paddingVertical: 24,
  },
  headerTitle: {
    marginLeft: '5%',
    width: '90%',
    marginTop: 120,
    height: 50,
    zIndex: 1,
  },
  headerDescription: {
    zIndex: 1,
  },
  contentContainer: {
    flex: 1,
    padding: 24,
  },
  activityContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  authoringInfoContainer: {
    flex: 1,
    marginHorizontal: 16,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
});
