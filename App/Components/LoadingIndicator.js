import React from 'react';
import { View, StyleSheet } from 'react-native';

import { StyleSheet } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

export default function LoadingIndicator(props) {
  const { enabled } = props;
  <Spinner
    visible={enabled}
    textContent={'Loading...'}
    textStyle={styles.spinnerTextStyle}
  />;
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
  },
});
