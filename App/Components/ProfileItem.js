import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from '@ui-kitten/components';
import { connect } from 'react-redux';
import MeActions, { MeSelectors } from '../Redux/MeRedux';
import { Images, ApplicationStyles, Colors, Fonts } from '../Themes/';

const ProfileItem = props => {
  const {
    memberCardName = 'Thành viên mới',
    navigation,
  } = props;
  const mePayload = _.get(props, 'mePayload') || {};
  const { userName = '', fullName = '', point = 0 } = mePayload;

  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={navigation ? 0.7 : 1}
      onPress={() => {
        navigation && navigation.navigate('ProfileDetailScreen');
      }}>
      <View style={styles.imageBox}>
        <Image
          style={styles.imageBox}
          source={{
            uri:
              'https://i.dlpng.com/static/png/1322579-child-avatar-icon-flat-design-red-yellow-coffee-avatar-png-693_720_preview.png',
          }}
          style={{ width: 60, height: 60, borderRadius: 30 }}
        />
      </View>
      <View style={styles.textBox}>
        <Text style={styles.text}>{fullName || userName}</Text>
        <Text style={styles.mcText}>{memberCardName}</Text>
      </View>
      <View style={styles.right}>
        <Image
          source={Images.coffeeBean}
          style={{ width: 25, height: 25, marginVertical: 5 }}
        />
        <Text
          style={{
            ...Fonts.style.h5,
            fontFamily: Fonts.type.semiBold,
            color: Colors.windowTint,
          }}>
          {point}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => ({
  mePayload: MeSelectors.getPayload(state.me),
});

export default connect(
  mapStateToProps,
  null,
)(ProfileItem);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  imageBox: {
    paddingLeft: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    paddingLeft: 10,
  },
  textBox: {
    flex: 1,
    alignItems: 'flex-start',
  },
  text: {
    paddingLeft: 20,
    fontFamily: Fonts.type.semiBold,
    color: Colors.textColor.black,
    fontSize: 16,
  },
  mcText: {
    paddingLeft: 20,
    color: Colors.textColor.tiny,
    fontFamily: Fonts.type.semiBold,
    fontSize: 14,
  },
  right: {
    // flex: 1,
    paddingHorizontal: 30,
    alignItems: 'center',
  },
});
