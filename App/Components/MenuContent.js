import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import {
  FontAwesome5,
  MaterialCommunityIcons,
  Ionicons,
} from '@expo/vector-icons';
import { Images, ApplicationStyles, Colors, Fonts } from '../Themes/';
import { LinearGradient } from 'expo-linear-gradient';

const MenuContent = props => {
  const { navigation } = props;
  const onPress1 = () => {
    // navigation.navigate('DevelopingScreen');
    const facebookParameters = "u=http://leecafeteria.com/"
    let url = 'https://www.facebook.com/sharer/sharer.php?'+facebookParameters;
    Linking.openURL(url).then((data) => {
    }).catch(() => {
      // alert('Something went wrong');
    });

  }
  const onPress2 = () => navigation.navigate('DevelopingScreen');
  const onPress3 = () => navigation.navigate('DevelopingScreen');
  const onPress4 = () => navigation.navigate('MyCouponScreen');

  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.content}>
        <TouchableOpacity
          style={styles.item}
          activeOpacity={0.8}
          onPress={onPress1}>
          <LinearGradient
            colors={[Colors.linearButton.start, Colors.linearButton.end]}
            style={styles.iconBox}>
            <Images.AddSvg
              height="30"
              width="30"
              fill={Colors.textColor.black}
            />
          </LinearGradient>
          <Text style={styles.text}>Mời bạn bè</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.item}
          activeOpacity={0.8}
          onPress={onPress2}>
          <LinearGradient
            colors={[Colors.linearButton.start, Colors.linearButton.end]}
            style={styles.iconBox}>
            <Images.JoystickSvg
              height="30"
              width="30"
              fill={Colors.textColor.black}
            />
          </LinearGradient>
          <Text style={styles.text}>Game</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.item}
          activeOpacity={0.8}
          onPress={onPress3}>
          <LinearGradient
            colors={[Colors.linearButton.start, Colors.linearButton.end]}
            style={styles.iconBox}>
            <View
              style={{
                width: 30,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Images.WalletSvg
                height="24"
                width="24"
                fill={Colors.textColor.black}
              />
            </View>
          </LinearGradient>
          <Text style={styles.text}>Nạp tiền</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.item}
          activeOpacity={0.8}
          onPress={onPress4}>
          <LinearGradient
            colors={[Colors.linearButton.start, Colors.linearButton.end]}
            style={styles.iconBox}>
            <Images.GiftSvg
              height="30"
              width="30"
              fill={Colors.textColor.black}
            />
          </LinearGradient>
          <Text style={styles.text}>Ưu đãi</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MenuContent;

const styles = StyleSheet.create({
  container: {
    // paddingTop: 30,
    paddingVertical: 10,
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  iconBox: {
    backgroundColor: 'transparent',
    padding: 10,
    borderRadius: 25,
    marginVertical: 10,
  },
  item: {
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 2,
    // android
    elevation: 5,
  },
  text: {
    ...Fonts.style.h4,
    color: Colors.textColor.black,
  },
});
