import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import ProfileItem from './ProfileItem';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Colors, Fonts } from '../Themes/';

///
function SettingItem(props) {
  const { item, navigation } = props;
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        item.to !== '' &&
          navigation.navigate(item.to, {
            ...item,
          });
      }}>
      <View style={styles.textbox}>
        <MaterialCommunityIcons
          style={styles.icon}
          size={25}
          color={'#000'}
          name={item.leftIcon}
        />
        <Text style={styles.text}>{item.name}</Text>
      </View>
      <View style={styles.right}>
        <MaterialCommunityIcons
          style={styles.icon}
          size={25}
          color={'#000'}
          name={item.rightIcon}
        />
      </View>
    </TouchableOpacity>
  );
}
SettingItem.propTypes = {};
export default SettingItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 70,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  textbox: {
    flexDirection: 'row',
  },
  icon: {
    paddingLeft: 10,
  },
  text: {
    paddingLeft: 20,
    fontFamily: Fonts.type.semiBold,
    fontSize: 16,
    color: Colors.textColor.black,
  },
  right: {
    paddingRight: 10,
  },
});
