import _ from 'lodash';
import React, { useState } from 'react';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  StatusBar,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux'
import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

const { height, width, fontScale, scale } = Dimensions.get('screen');


function MemberInfoScreen(props) {
  const { navigation, route } = props;
  if (navigation) {
    navigation.setOptions({
      title: _.get(route, 'params.name', null),
    });
  }
  const mePayload = _.get(props, 'mePayload') || {};
  const userName = _.get(mePayload, 'userName');


  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView style={styles.container}>
          <LinearGradient
            colors={[Colors.linear.start, Colors.linear.end]}
            style={styles.card}>
            <Text style={styles.cardNameText}>{userName}</Text>
            <Text style={styles.cardmemberText}>Thanh vien moi</Text>
          </LinearGradient>
          <View style={styles.current}>
            <Text style={styles.currentNameText}>
              Mỗi lần thanh toán, bạn sẽ nhận
            </Text>
            <View style={styles.currentCenter}>
              <Text style={styles.currentCenterText}>1.000đ = 1 </Text>
              <Image
                source={Images.coffeeBean}
                style={{ width: 16, height: 16 }}
              />
            </View>
            <Text style={styles.currentdescriptionText}>
              Hãy tích lũy thật nhiều BEAN để nhận được những ưu đãi hấp dãn
              nhé.
            </Text>
          </View>
          {/* <MyTabs /> */}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}



MemberInfoScreen.propTypes = {};

const mapStateToProps = state => ({
  mePayload: MeSelectors.getPayload(state.me),
});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MemberInfoScreen);


function Tab1(props) {
  const routeName = props.route.name;
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        marginBottom: 20,
        padding: 10,
      }}>
      <Text style={{}}>{`Bạn đã đạt được hạng ${routeName}`}</Text>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 10,
        }}>
        <Ionicons name="md-apps" size={12} style={{ paddingHorizontal: 10 }} />
        <Text style={{ fontSize: 12 }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 10,
        }}>
        <Ionicons name="md-apps" size={12} style={{ paddingHorizontal: 10 }} />
        <Text style={{ fontSize: 12 }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 10,
        }}>
        <Ionicons name="md-apps" size={12} style={{ paddingHorizontal: 10 }} />
        <Text style={{ fontSize: 12 }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
        </Text>
      </View>
    </View>
  );
}

const Tab = createMaterialTopTabNavigator();
function MyTabs(props) {
  return (
    <Tab.Navigator
      tabBarOptions={{
        // activeTintColor: '#3378F6',
        activeTintColor: 'black',
        allowFontScaling: false,
        labelStyle: {
          ...Fonts.style.h6,
          fontFamily: Fonts.type.bold,
        },
        // tabStyle: { },
      }}>
      <Tab.Screen
        name="Mới"
        component={Tab1}
        options={{
          title: 'Mới',
        }}
      />
      <Tab.Screen
        name="Vàng"
        component={Tab1}
        options={{
          title: 'Vàng',
        }}
      />
      <Tab.Screen
        name="Kim Cương"
        component={Tab1}
        options={{
          title: 'Kim Cương',
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
    backgroundColor: Colors.block,
  },
  container: {
    paddingHorizontal: 15,
    paddingTop: 20,
    flex: 1,
  },
  //
  card: {
    height: 150,
    width: '100%',
    backgroundColor: Colors.yellow,
    justifyContent: 'flex-end',
    paddingVertical: 10,
    paddingHorizontal: 10,

    borderRadius: 10,

    shadowColor: 'black',
    shadowOpacity: 0.4,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 2,
    elevation: 5,
  },
  cardNameText: {
    ...Fonts.style.h3,
    fontFamily: Fonts.type.extraBold,
    color: Colors.white,
    paddingVertical: 2,
  },
  cardmemberText: {
    ...Fonts.style.h5,
    fontFamily: Fonts.type.semiBold,
    color: Colors.white,
    paddingVertical: 2,
  },

  current: {
    marginVertical: 20,
    padding: 10,
    borderRadius: 10,
    borderWidth: 0.1,
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  currentNameText: {
    ...Fonts.style.h3,
    // fontFamily: Fonts.type.semiBold,
    paddingVertical: 8,
    textAlign: 'center',
    // color: Colors.textColor.black,
  },

  currentCenter: {
    width: '60%',
    backgroundColor: Colors.white,
    borderColor: '#3378F6',
    paddingHorizontal: 5,
    marginVertical: 5,
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  currentCenterText: {
    ...Fonts.style.h3,
    color: '#3378F6',
    fontFamily: Fonts.type.bold,
  },
  currentdescriptionText: {
    ...Fonts.style.h4,
    // fontFamily: Fonts.type.light,
    paddingTop: 10,
    textAlign: 'center',
    // color: Colors.textColor.black,
  },

  //
});
