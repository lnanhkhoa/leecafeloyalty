import React, { Component, useState, useEffect } from 'react';
import _ from 'lodash';
import moment from 'moment';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';

import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import TransactionActions, {
  TransactionSelectors,
} from '../../Redux/TransactionRedux';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

const { height, width, fontScale, scale } = Dimensions.get('screen');

const typeTransaction = [
  'Withdraw',
  'Nạp tiền',
  'Thanh toán',
  'Điểm thưởng',
  'Hoàn lại',
  'Chuyển đổi',
];


function HistoryPaymentScreen(props) {
  const {
    navigation,
    route,
    transactionPayload,
    mePayload,
    meData,
    getTransaction,
  } = props;

  const { Authorization } = meData;

  if (navigation) {
    navigation.setOptions({
      title: _.get(route, 'params.name', null),
    });
  }

  const transactionData = _.get(transactionPayload, 'data', []);
  const userId = _.get(mePayload, 'id', null);

  useEffect(() => {
    getTransaction({
      pageSize: 30,
      pageIndex: 0,
      userId: userId,
      Authorization,
    });
    return () => {};
  }, []);

  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <View
          style={{
            // paddingTop: 30,
            paddingHorizontal: 20,
            paddingBottom: 10,
          }}>
          {/* <Text style={{ ...Fonts.style.h3, color: Colors.textColor.black }}>
            Lịch sử giao dịch
          </Text> */}
        </View>
        <FlatList
          style={styles.container}
          keyExtractor={(item, index) => item.id}
          data={transactionData}
          contentContainerStyle={{ paddingBottom: 20 }}
          renderItem={({ item }) => <HistoryItem item={item} />}
        />
      </SafeAreaView>
    </View>
  );
}

HistoryPaymentScreen.propTypes = {};

const mapStateToProps = state => ({
  transactionPayload: TransactionSelectors.getPayload(state.transaction),
  meData: MeSelectors.getData(state.me),
  mePayload: MeSelectors.getPayload(state.me),
});

const mapDispatchToProps = dispatch => {
  return {
    getTransaction: data =>
      dispatch(TransactionActions.transactionRequest(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HistoryPaymentScreen);

const HistoryItem = props => {
  const { item } = props;

  const date = moment(item.createdDate).add(7, 'hour').format('HH:mm DD/MM/YYYY');
  const subText = _.includes([0, 2], item.type) ? '-' : '+';

  return (
    <View style={styles.box}>
      <View>
        <Text style={styles.address}>{typeTransaction[item.type]}</Text>
        <Text style={styles.status}>{item.status || 'Đã hoàn tất'}</Text>
        <Text style={styles.date}>{date}</Text>
      </View>
      <View style={{ alignItems: 'flex-end' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text
            style={[
              styles.bean,
              {
                color:
                  subText === '+' ? Colors.greenMedium : Colors.bloodOrange,
              },
            ]}>
            {subText + item.amount}
          </Text>
          <Image source={Images.coffeeBean} style={{ width: 16, height: 16 }} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
    backgroundColor: Colors.block,
  },
  container: {
    paddingHorizontal: 20,
    paddingTop: 10,
    flex: 1,
    // backgroundColor: Colors.block,
  },
  // item
  box: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    marginVertical: 4,
  },

  address: {
    ...Fonts.style.h4,
    fontFamily: Fonts.type.semiBold,
    paddingVertical: 2,
  },
  status: {
    ...Fonts.style.h5,
    fontFamily: Fonts.type.semiBold,
    color: Colors.bloodOrange,
    paddingVertical: 2,
  },
  date: {
    ...Fonts.style.h4,
    fontFamily: Fonts.type.light,
    paddingVertical: 2,
  },
  bean: {
    ...Fonts.style.h4,
    fontFamily: Fonts.type.semiBold,
    color: Colors.bloodOrange,
    paddingVertical: 2,
  },
  total: {
    ...Fonts.style.h4,
    fontFamily: Fonts.type.semiBold,
    paddingVertical: 2,
  },
});
