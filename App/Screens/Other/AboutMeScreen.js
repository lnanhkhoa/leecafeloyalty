import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
  Dimensions,
  SafeAreaView,
} from 'react-native';

import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes';
import { Ionicons, FontAwesome5, AntDesign } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
{
  /* <source src="http://leecafeteria.com/wp-content/uploads/2019/11/video-lee-cafe.mp4" type="video/mp4"></source> */
}
const { height, width, fontScale, scale } = Dimensions.get('screen');

function AboutMeScreen(props) {
  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={{ paddingVertical: 50 }}>
          <View style={styles.image}>
            <Image
              style={styles.imageBox}
              source={require('../../../assets/logo.jpg')}
            />
          </View>
          <View style={styles.container}>
            <Text style={[styles.desc, { textAlign: 'justify' }]}>
              Là thương hiệu máy pha chế và bán hàng tự động hiện đại hàng đầu
              Việt Nam. Lee Cafeteria 4.0 mang đến cho bạn giải pháp kinh doanh
              tự động hoá trong ngành đồ uống.
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              paddingVertical: 10,
              marginVertical: 10,
              marginHorizontal: 10,
              borderRadius: 10,
              backgroundColor: Colors.white,
            }}>
            <View
              style={{
                // paddingHorizontal: 20,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingVertical: 5,
              }}>
              <AntDesign
                name="home"
                size={25}
                style={{ paddingHorizontal: 10 }}
              />
              <Text style={styles.desc1}>
                Địa chỉ: Số 7, Đường Trần Hưng Đạo, Phường Hiệp Phú, Quận 9,
                Tp.HCM
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingVertical: 5,
              }}>
              <AntDesign
                name="phone"
                size={25}
                style={{ paddingHorizontal: 10 }}
              />
              <Text style={styles.desc1}>
                Hotline: 0902 84 25 26 - 0938 80 90 95
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingVertical: 5,
              }}>
              <AntDesign
                name="mail"
                size={25}
                style={{ paddingHorizontal: 10 }}
              />
              <Text style={styles.desc1}>Email: leecafeteria@gmail.com</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingVertical: 5,
              }}>
              <AntDesign
                name="earth"
                size={25}
                style={{ paddingHorizontal: 10 }}
              />
              <Text style={styles.desc1}>Website: leecafeteria.com</Text>
            </View>
          </View>
          <Text style={{ alignSelf: 'center' }}>
            {'Copyright © 2019 LEE CAFETERIA 4.0'}
          </Text>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

export default AboutMeScreen;

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
    backgroundColor: Colors.block,
  },
  container: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 10,
    flex: 1,
    backgroundColor: Colors.white,
  },
  // item
  image: {
    alignItems: 'center',
    paddingVertical: 30,
    marginVertical: 10,

    backgroundColor: Colors.main,
  },
  imageBox: {
    width: width,
    height: (267 / 1704) * width,
  },

  desc: {
    ...Fonts.style.h4,
    fontFamily: Fonts.type.light,
  },
  desc1: {
    width: '90%',
    paddingHorizontal: 10,
    textAlign: 'right',
    ...Fonts.style.h4,
    fontFamily: Fonts.type.base,
  },
});
