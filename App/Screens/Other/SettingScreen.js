import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes/';

// components
// import { ArrowIosBackIcon } from '../../Components/Icons';
import ProfileItem from '../../Components/ProfileItem';
import SettingItem from '../../Components/SettingItem';

import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux';

SettingScreen.propTypes = {};
function SettingScreen(props) {
  const { navigation, signOut } = props;

  const userInfo = {
    avatar: '',
    name: 'Lê Khoa',
    memberCardName: 'Thành viên Vàng',
  };

  const getSignOut = () => {
    signOut();
  };

  return (
    <View style={styles.mainContainer}>
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={styles.content}>
          <ProfileItem item={userInfo} navigation={navigation} />
          <FlatList
            data={data}
            keyExtractor={(item, index) => item.id}
            ListHeaderComponentStyle={{
              marginBottom: 10,
              backgroundColor: Colors.block,
            }}
            ListHeaderComponent={<View />}
            ListFooterComponent={
              <TouchableOpacity onPress={getSignOut} style={{ padding: 10 }}>
                <Text style={styles.text}>Đăng xuất</Text>
              </TouchableOpacity>
            }
            renderItem={({ item }) => {
              return <SettingItem item={item} navigation={navigation} />;
            }}
          />
        </View>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  content: {
    flex: 1,
    // paddingHorizontal: 15,
    backgroundColor: Colors.block,
  },
  flatList: {
    width: '100%',
  },
  text: {
    paddingLeft: 20,
    fontFamily: Fonts.type.semiBold,
    fontSize: 16,
    color: Colors.textColor.black,
  },
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(LoginActions.signOut()),
});

export default connect(
  null,
  mapDispatchToProps,
)(SettingScreen);

const data = [
  {
    id: '1',
    leftIcon: 'account-outline',
    rightIcon: 'chevron-right',
    name: 'Thông tin thành viên',
    to: 'MemberInfoScreen',
  },
  {
    id: '5',
    leftIcon: 'card-text-outline',
    rightIcon: 'chevron-right',
    name: 'Chính sách và Quyền lợi',
    to: '',
  },
  {
    id: '6',
    leftIcon: 'history',
    rightIcon: 'chevron-right',
    name: 'Lịch sử giao dịch',
    to: 'HistoryPaymentScreen',
  },
  {
    id: '4',
    leftIcon: 'settings-outline',
    rightIcon: 'chevron-right',
    name: 'Cài đặt',
    to: '',
  },
  {
    id: '6',
    leftIcon: 'comment-account-outline',
    rightIcon: 'chevron-right',
    name: 'Về chúng tôi',
    to: 'AboutMeScreen',
  },
  {
    id: '3',
    leftIcon: 'help',
    rightIcon: 'chevron-right',
    name: 'Trợ giúp',
    to: '',
  },
];
