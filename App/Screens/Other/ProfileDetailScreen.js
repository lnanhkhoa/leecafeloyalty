import React, { Component, useState } from 'react';
import _ from 'lodash';
import moment from 'moment';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
  Dimensions,
  SafeAreaView,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo-linear-gradient';

import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import { KeyboardAvoidingView } from '../Auth/extra/3rd-party';

const UserItem = props => {
  const { type, name, onChangeName, editable } = props;
  return (
    <View
      style={{
        width: '100%',
        marginVertical: 4,
        paddingVertical: 15,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: Colors.block,
        //
        borderRadius: 5,
      }}>
      <Text>{type}</Text>
      <TextInput style={{ ...Fonts.size.h4, fontFamily: Fonts.type.bold }}
        value={name}
        editable={editable}
        onChangeText={onChangeName}
      />
    </View>
  );
};

function ProfileDetailScreen(props) {
  const { mePayload = {} } = props;
  const { navigation, route } = props;
  // const { userName, email, fullName, phoneNumber, createdDate } = mePayload;
  const onPressSave = () => { navigation && navigation.goBack()}

  const [userName, setuserName] = useState(mePayload.userName);
  const [email, setemail] = useState(mePayload.email);
  const [fullName, setfullName] = useState(mePayload.fullName|| "");
  const [phoneNumber, setphoneNumber] = useState(mePayload.phoneNumber);
  const [createdDate, setcreatedDate] = useState(mePayload.createdDate);



  return (
    <KeyboardAvoidingView>
      <View style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ paddingVertical: 20 }}>
            <View style={styles.image}>
              <Image
                style={styles.imageBox}
                source={{
                  uri:
                    'https://i.dlpng.com/static/png/1322579-child-avatar-icon-flat-design-red-yellow-coffee-avatar-png-693_720_preview.png',
                }}
              // style={{ width: 128, height: 129, borderRadius: 999 }}
              />
            </View>
            <View style={styles.container}>
              <UserItem type="Họ và tên" name={fullName} onChangeName={setfullName}/>
              <UserItem type="Username" name={userName} onChangeName={setuserName}/>
              <UserItem type="Email" name={email} onChangeName={setemail}/>
              <UserItem type="Số điện thoại" name={phoneNumber} onChangeName={setphoneNumber}/>
              <UserItem type="Ngày tạo" name={moment(createdDate).format('DD/MM/YYYY')} 
              editable={false} />
            </View>
            <View style={styles.buttonBox}>
              <TouchableOpacity onPress={onPressSave}>
                <LinearGradient
                  colors={[Colors.linearButton.start, Colors.linearButton.end]}
                  style={styles.button}>
                  <Text style={{ ...Fonts.style.h3, fontWeight: 'bold', color: Colors.black }}>Save</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    </KeyboardAvoidingView>
  );
}

const mapStateToProps = state => ({
  mePayload: MeSelectors.getPayload(state.me),
});

const mapDispatchToProps = dispatch => ({
  getMe: params => dispatch(MeActions.meRequest({ ...params })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileDetailScreen);

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
    backgroundColor: Colors.white,
  },
  container: {
    paddingHorizontal: 20,
    paddingTop: 10,
    flex: 1,
    // backgroundColor: Colors.block,
  },
  // item
  image: {
    // justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  imageBox: {
    width: 128,
    height: 128,
    borderRadius: 999,
  },
  buttonBox: {
    paddingHorizontal: 25,
    paddingVertical: 10,
  },
  button: {
    borderRadius: 10,
    backgroundColor: 'red',
    alignItems: 'center',
    paddingVertical: 10
  }
});
