import React, { useState, useRef } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Linking,
  FlatList,
} from 'react-native';
import MapView, {
  ProviderPropType,
  Marker,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import { Autocomplete, Icon, Text as UKText } from '@ui-kitten/components';
import * as Permission from 'expo-permissions'

import {
  Images,
  Metrics,
  ApplicationStyles,
  Colors,
  Fonts,
} from '../../Themes/';
import MachineActions, { MachineSelectors } from '../../Redux/MachineRedux';

const screen = Dimensions.get('screen');

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0461;
const LONGITUDE_DELTA = 0.0213;

function LockScreen(props) {
  const { navigation, machinePayload } = props;

  const machinePayloadData = _.get(machinePayload, 'data', []);
  const DATA_SEARCH = _.map(machinePayloadData, item => ({
    ...item,
    image: CoffeeImages[_.random(0, 4)],
    id: String(item.machineCode),
    title: item.address,
  }));
  const mapviewRef = useRef(null);
  const [search, setsearch] = useState('');
  const [dataSearch, setDataSearch] = React.useState(DATA_SEARCH);
  const [itemSelected, setItemSelected] = useState(DATA_SEARCH[0]);

  const clearInput = () => onChangeText('');
  const onSelect = ({ title }) => {
    setsearch(title);
    const item = _.find(DATA_SEARCH, item => item.title === title);
    setItemSelected(item);
    !!item &&
      mapviewRef.current.animateToRegion({
        latitude: item.latitude,
        longitude: item.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      });
  };
  const onChangeText = query => {
    setsearch(query);
    if (query === '') {
      setDataSearch(DATA_SEARCH);
      return null;
    }
    setDataSearch(
      DATA_SEARCH.filter(item =>
        item.title.toLowerCase().includes(query.toLowerCase()),
      ),
    );
  };

  React.useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      position => {
        mapviewRef && mapviewRef.current.animateToRegion({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        });
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }, [])




  const initialRegion = {
    latitude: 10.822646,
    longitude: 106.770745,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  };

  return (
    <View style={styles.mainContainer}>
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.content}>
          <MapView
            ref={mapviewRef}
            style={styles.mapStyle}
            provider="google"
            initialRegion={initialRegion}
            showsUserLocation={true}
            showsMyLocationButton
            cacheEnabled={true}
            loadingEnabled
            pitchEnabled>
            {DATA_SEARCH.length > 0 &&
              DATA_SEARCH.map(item => {
                return (
                  <Marker
                    image={
                      itemSelected && itemSelected.id === item.id
                        ? Images.pinSelectedMarker
                        : Images.pinMarker
                    }
                    onPress={() => {
                      onSelect({ title: item.title });
                    }}
                    key={item.id}
                    coordinate={{
                      latitude: item.latitude,
                      longitude: item.longitude,
                    }}
                  />
                );
              })}
          </MapView>
          <FlatList
            horizontal
            keyExtractor={item => item.id}
            initialNumToRender={4}
            contentContainerStyle={{ paddingHorizontal: 50 }}
            style={styles.scrollView}
            showsHorizontalScrollIndicator={false}
            data={DATA_SEARCH}
            renderItem={({ item }) => (
              <LockItem item={item} navigation={navigation} />
            )}
          />
          <View style={styles.searchBarBox}>
            <Autocomplete
              style={styles.searchBar}
              value={search}
              data={dataSearch}
              onChangeText={onChangeText}
              onIconPress={clearInput}
              placeholder="Tìm kiếm"
              icon={CloseIcon}
              onSelect={onSelect}
            />
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
}

LockScreen.propTypes = {};

const mapStateToProps = state => ({
  machinePayload: MachineSelectors.getPayload(state.machine),
});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LockScreen);

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  scrollView: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    // paddingVertical: 10,
  },
  searchBarBox: {
    position: 'absolute',
    top: 30,
    alignItems: 'center',
  },
  searchBar: {
    minWidth: screen.width * 0.8,
  },
});

const CloseIcon = style => <Icon {...style} name="close" />;

const SCHEMA_GG_MAPS = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
const SLIDER_HEIGHT_CAROUSEL = screen.height * 0.3;
const DEFAULT_IMAGE_STORE =
  'http://leecafeteria.com/wp-content/uploads/2019/11/6.png';
function LockItem(props) {
  const { item = {}, navigation } = props;
  const { title, address, image = Images.coffeelock1 } = item;
  return (
    <TouchableOpacity
      style={{
        borderRadius: 12,
        backgroundColor: Colors.white,
        overflow: 'hidden',
        width: (screen.width * 64) / 100,
        marginRight: 8,
      }}
      activeOpacity={0.9}
      onPress={() => {
        // const latLng = `${item.latitude},${item.longitude}`;
        // const label = item.title;
        // const url = Platform.select({
        //   ios: `${SCHEMA_GG_MAPS}${label}@${latLng}`,
        //   android: `${SCHEMA_GG_MAPS}${latLng}(${label})`
        // });
        // Linking.openURL(url);
        navigation && navigation.navigate('LockDetailScreen', { item });
      }}>
      <Image
        style={{
          height: SLIDER_HEIGHT_CAROUSEL * 0.5,
          backgroundColor: Colors.grayLight,
          width: (screen.width * 64) / 100,
        }}
        source={image || { uri: DEFAULT_IMAGE_STORE }}
      />
      <View style={styles.item_banner}>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '500',
            padding: 6,
            color: Colors.black,
          }}
          numberOfLine={1}>
          {title}
        </Text>
        <Text
          allowFontScaling={true}
          style={{
            fontSize: 14,
            color: Colors.black,
            padding: 6,
          }}
          numberOfLines={2}
          ellipsizeMode="tail">
          {address}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const CoffeeImages = [
  Images.coffeelock1,
  Images.coffeelock2,
  Images.coffeelock3,
  Images.coffeelock4,
];

// const DATA_SEARCH = [
//   {
//     id: '1',
//     title: 'Trần Hưng Đạo, Hiệp Phú , Q.9',
//     address:
//       'Số 7 Trần Hưng Đạo, Hiệp Phú, Quận 9, Hồ Chí Minh ( Cửa hàng café tự động )',
//     image: Images.coffeelock1,
//     latitude: 10.8472023,
//     longitude: 106.7725283,
//   },
//   {
//     id: '2',
//     title: 'Võ văn Ngân, Q.Thủ Đức',
//     address:
//       'Số 281, Võ Văn Ngân, Quận Thủ Đức, Hồ Chí Minh ( Nhà văn hoá thiếu nhi quận Thủ Đức )',
//     image: Images.coffeelock2,
//     latitude: 10.8508373,
//     longitude: 106.7641063,
//   },
//   {
//     id: '3',
//     title: 'DH Văn Hoá, Q.9',
//     address:
//       'Số 288, Đỗ Xuân Hợp, Quận 9, Hồ Chí Minh ( Trường đại học Văn Hoá cơ sở 2 )',
//     image: Images.coffeelock3,
//     latitude: 10.8229013,
//     longitude: 106.7683053,
//   },
//   {
//     id: '4',
//     title: 'Ngân hàng ACB, Q.3',
//     address:
//       'Số 442, Nguyễn Thị Minh Khai, Quận 3, Hồ Chí Minh ( Hội sở ngân hàng ACB )',
//     image: Images.coffeelock4,
//     latitude: 10.7695893,
//     longitude: 106.6828433,
//   },
// ];
