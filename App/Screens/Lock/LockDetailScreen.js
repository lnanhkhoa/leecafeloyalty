import React from 'react';
import {
  Image,
  ImageSourcePropType,
  ImageStyle,
  ListRenderItemInfo,
  ScrollView,
  View,
} from 'react-native';
import {
  Button,
  Card,
  Icon,
  List,
  StyleService,
  Text,
  useStyleSheet,
} from '@ui-kitten/components';
import { ImageOverlay } from './extra/image-overlay.component';

const IMAGES = [
  {
    uri:
      'http://leecafeteria.com/wp-content/uploads/2019/10/f2c0effa1147f619af56-300x225.jpg',
  },
  {
    uri:
      'http://leecafeteria.com/wp-content/uploads/2019/10/ec389b0d65b082eedba1-225x300.jpg',
  },
  {
    uri:
      'http://leecafeteria.com/wp-content/uploads/2019/10/dca5889f7622917cc833-225x300.jpg',
  },
  {
    uri:
      'http://leecafeteria.com/wp-content/uploads/2019/10/b301c2f63b4bdc15855a-225x300.jpg',
  },
  {
    uri:
      'http://leecafeteria.com/wp-content/uploads/2019/10/af2b8d1e73a394fdcdb2-225x300.jpg',
  },
];

export default ({ navigation, route }) => {
  const styles = useStyleSheet(themedStyles);
  const routeParams = route.params;
  const { item = {} } = routeParams;

  const renderImageItem = info => (
    <Image style={styles.imageItem} source={info.item} />
  );

  navigation.setOptions({
    title: item.title,
  });

  return (
    <ScrollView style={styles.container}>
      <ImageOverlay style={styles.image} source={item.image} />
      <Card style={styles.bookingCard} appearance="filled" disabled={true}>
        <Text style={styles.title} category="h6">
          {item.title}
        </Text>
        <Text style={styles.rentLabel} appearance="hint" category="p2">
          Địa chỉ
        </Text>
        <Text style={styles.priceLabel} category="p1">
          {item.address}
        </Text>
      </Card>
      <Text style={styles.sectionLabel} category="s1">
        Hình ảnh
      </Text>
      <List
        contentContainerStyle={styles.imagesList}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        data={IMAGES}
        renderItem={renderImageItem}
      />
    </ScrollView>
  );
};

const themedStyles = StyleService.create({
  container: {
    backgroundColor: 'background-basic-color-2',
  },
  image: {
    height: 300,
  },
  bookingCard: {
    marginTop: -80,
    margin: 16,
  },
  title: {
    width: '95%',
  },
  rentLabel: {
    marginTop: 24,
  },
  priceLabel: {
    marginTop: 8,
  },
  bookButton: {
    position: 'absolute',
    bottom: 24,
    right: 24,
  },
  detailsList: {
    flexDirection: 'row',
    marginHorizontal: -4,
    marginVertical: 8,
  },
  detailItem: {
    marginHorizontal: 4,
    borderRadius: 16,
  },
  optionList: {
    flexDirection: 'row',
    marginHorizontal: -4,
    marginVertical: 8,
  },
  optionItem: {
    marginHorizontal: 4,
    paddingHorizontal: 0,
  },
  description: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  sectionLabel: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  imagesList: {
    padding: 8,
    marginBottom: 50,
    backgroundColor: 'background-basic-color-2',
  },
  imageItem: {
    width: 180,
    height: 120,
    borderRadius: 8,
    marginHorizontal: 8,
  },
});
