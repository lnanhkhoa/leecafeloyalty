import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import {Colors} from '../Themes/'

LoadingIndicator.propTypes = {
  enabled: PropTypes.bool,
};

export function LoadingIndicator(props) {
  const { enabled = false } = props;
  return (
    <Spinner
      visible={enabled}
      textContent={'Đang xử lý ...'}
      textStyle={styles.spinnerTextStyle}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
  },
  spinnerTextStyle: {
    color: Colors.text,
  },
});
