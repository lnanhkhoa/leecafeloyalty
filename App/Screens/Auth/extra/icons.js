import React from 'react';
import { ImageStyle } from 'react-native';
import { Icon, IconElement } from '@ui-kitten/components';

export const EyeIcon = style => <Icon {...style} name="eye" />;

export const EyeOffIcon = style => <Icon {...style} name="eye-off" />;

export const PersonIcon = style => <Icon {...style} name="person" />;

export const EmailIcon = style => <Icon {...style} name="email" />;

export const PhoneIcon = style => <Icon {...style} name="phone" />;

export const PlusIcon = style => <Icon {...style} name="plus" />;
