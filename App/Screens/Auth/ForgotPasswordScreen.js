import React from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import { Button, Input, Text } from '@ui-kitten/components';
import { ImageOverlay } from './extra/image-overlay.component';
import { EmailIcon } from './extra/icons';
import { KeyboardAvoidingView } from './extra/3rd-party';

export default ({ navigation }) => {
  const [email, setEmail] = React.useState();

  const onResetPasswordButtonPress = () => {
    navigation && navigation.goBack();
  };

  return (
    <KeyboardAvoidingView>
      <ImageOverlay
        style={styles.container}
        source={require('./assets/image-background.jpg')}>
        <SafeAreaView style={{ flex: 1 }}>
          <Text
            style={styles.forgotPasswordLabel}
            category="h4"
            status="control">
            Forgot Password
          </Text>
          <Text style={styles.enterEmailLabel} status="control">
            Please enter your email address
          </Text>
          <View style={styles.formContainer}>
            <Input
              status="control"
              placeholder="Email"
              icon={EmailIcon}
              value={email}
              onChangeText={setEmail}
            />
          </View>
          <Button
            style={styles.resetPasswordButton}
            size="giant"
            onPress={onResetPasswordButtonPress}>
            RESET PASSWORD
          </Button>
        </SafeAreaView>
      </ImageOverlay>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'space-between',
    marginTop: 24,
  },
  forgotPasswordLabel: {
    zIndex: 1,
    alignSelf: 'center',
    marginTop: 40,
  },
  enterEmailLabel: {
    zIndex: 1,
    alignSelf: 'center',
    marginTop: 64,
  },
  resetPasswordButton: {
    marginVertical: 20,
    marginHorizontal: 16,
  },
});
