import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes/';

SplashScreen.propTypes = {};
export default SplashScreen;
function SplashScreen(props) {
  const { navigation } = props;

  useEffect(() => {
    navigation.push('Login');
    return () => {};
  }, []);

  return (
    <View style={styles.mainContainer}>
      <View style={styles.content}>{/* <Text>SplashScreen</Text> */}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
