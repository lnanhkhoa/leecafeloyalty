import React, { Component, useState, useRef } from 'react';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  StatusBar,
  Dimensions,
  useWindowDimensions,
  TouchableOpacity,
} from 'react-native';
import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes/';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { Button, Icon } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import Swiper from 'react-native-swiper';

export default function OnboardingScreen(props) {
  const { navigation } = props;
  const { width, height } = useWindowDimensions();
  const [textButton, settextButton] = useState("Tiếp tục");
  const [indexSwiper, setindexSwiper] = useState(0);
  const swiperRef = useRef(null);

  const onPress = () => {
    if (indexSwiper === 2) navigation.push('Login');
    swiperRef.current.scrollBy((indexSwiper + 1) % 3)
  }
  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <View style={styles.content}>
          <Swiper
            ref={swiperRef}
            // index={indexSwiper}
            onIndexChanged={(index) => {
              setindexSwiper(index)
              settextButton(index === 2 ? "Bắt đầu" : "Tiếp tục")
            }}
            style={styles.wrapper}
            dot={<View style={ApplicationStyles.swiper.dot} />}
            activeDot={<View style={ApplicationStyles.swiper.dotActive} />}
            paginationStyle={{
              bottom: ApplicationStyles.utils.resizeHeight(30),
            }}
            loop={false}>
            {/* slide1 */}
            <View style={styles.slide}>
              <Text style={styles.titleText}>Dễ dàng, tiện lợi</Text>
              <Image
                source={Images.tienLoi}
                style={{
                  height: 250,
                  width: '100%',
                }}
              />
              <Text style={styles.descriptionText}>
                Bạn có thể dễ dàng mua tại cái máy bán lẻ luôn luôn hoạt động 24
                giờ mỗi ngày
              </Text>
            </View>
            {/* slide2 */}
            <View style={styles.slide}>
              <Text style={styles.titleText}>Đa dạng sản phẩm</Text>
              <Image
                source={Images.dadangCafe}
                style={{
                  height: 250,
                  width: '100%',
                }}
              />
              <Text style={styles.descriptionText}>
                Có rất nhiều sản phẩm như cà phê, trà sữa, cacao...
              </Text>
            </View>
            {/* slide3 */}
            <View style={styles.slide}>
              <Text style={styles.titleText}>Thanh toán dễ dàng</Text>
              <Image
                source={Images.paymentCafe}
                style={{
                  height: 250,
                  width: '100%',
                }}
              />
              <Text style={styles.descriptionText}>
                Hỗ trợ thanh toán qua các kênh thanh toán khác nhau như Momo,
                VNPay và thông qua đổi điểm Bean.
              </Text>
            </View>
          </Swiper>
          <View style={styles.submitBox}>
            <View style={styles.submitButton}>
              <Button
                title={textButton}
                type="outline"
                color={Colors.grayDark}
                onPress={onPress}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    marginTop: ApplicationStyles.utils.resizeHeight(50),
  },
  submitBox: {
    paddingBottom: 20,
  },
  submitButton: {
    marginHorizontal: ApplicationStyles.utils.resizeHeight(28),
  },
  submitButtonText: {
    backgroundColor: 'transparent',
    ...Fonts.style.h3,
    letterSpacing: 0.3,
    fontFamily: Fonts.type.bold,
    color: Colors.textColor.white,
    padding: 16,
  },
  // swiper
  wrapper: {
    // marginHorizontal: 1,
  },

  slide: {
    flex: 1,
    paddingHorizontal: ApplicationStyles.utils.resizeHeight(28),
    // justifyContent: 'center'
  },
  titleText: {
    ...Fonts.style.h1,
    fontFamily: Fonts.type.semiBold,
    color: Colors.textColor.black,
    paddingVertical: 10,
  },
  descriptionText: {
    marginTop: ApplicationStyles.utils.resizeHeight(66),
    ...Fonts.style.h5,
    textAlign: 'center',
    color: Colors.textColor.tiny,
  },
  image: {
    marginTop: ApplicationStyles.utils.resizeHeight(67),
    width: ApplicationStyles.utils.resizeWidth(329),
    height: ApplicationStyles.utils.resizeHeight(183),
    backgroundColor: Colors.main,
    opacity: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
