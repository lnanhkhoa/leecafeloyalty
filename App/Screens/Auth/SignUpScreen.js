import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  CheckBox,
  Input,
  StyleService,
  useStyleSheet,
} from '@ui-kitten/components';
import {
  EmailIcon,
  EyeIcon,
  EyeOffIcon,
  PhoneIcon,
  PersonIcon,
  PlusIcon,
} from './extra/icons';
import { FontAwesome } from '@expo/vector-icons';
import { ImageOverlay } from './extra/image-overlay.component';
import { ProfileAvatar } from './extra/profile-avatar.component';
import { KeyboardAvoidingView } from './extra/3rd-party';
import { LoadingIndicator } from '../LoadingIndicator';
import { Colors, Images } from '../../Themes';

import { MeSelectors } from '../../Redux/MeRedux';
import SignUpActions, { SignUpSelectors } from '../../Redux/SignUpRedux';

const mapStateToProps = state => ({
  signUpPayload: SignUpSelectors.getPayload(state.signUp),
  signUpFetching: SignUpSelectors.getFetching(state.signUp),
  mePayload: MeSelectors.getPayload(state.me),
  meFetching: MeSelectors.getFetching(state.me),
});

const mapDispatchToProps = dispatch => {
  return {
    signUp: params => dispatch(SignUpActions.signUpRequest({ ...params })),
    getMe: params => dispatch(MeActions.meRequest({ ...params })),
  };
};

function SignUpScreen(props) {
  const {
    navigation,
    signUp,
    signUpPayload,
    meFetching,
    signUpFetching,
    getMe,
    mePayload,
  } = props;

  const loadingIndicatorEnabled = meFetching || signUpFetching;
  const styles = useStyleSheet(themedStyles);
  const [userName, setUserName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = React.useState('');
  const [termsAccepted, setTermsAccepted] = React.useState(false);
  const [passwordVisible, setPasswordVisible] = React.useState(false);

  useEffect(() => {
    return () => {};
  }, []);

  const onSignUpButtonPress = e => {
    signUp({
      username: userName,
      password: password,
      email: email,
      phoneNumber: phoneNumber,
    });
  };

  const onSignInButtonPress = () => {
    navigation && navigation.navigate('Login');
  };
  const onGoBack = () => {
    navigation && navigation.goBack();
  };

  const onPasswordIconPress = () => {
    setPasswordVisible(!passwordVisible);
  };

  const renderEditAvatarButton = () => (
    <Button style={styles.editAvatarButton} status="basic" icon={PlusIcon} />
  );

  return (
    <KeyboardAvoidingView>
      <LoadingIndicator enabled={loadingIndicatorEnabled} />
      <ImageOverlay style={styles.container} source={Images.signupBackground}>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity style={styles.backIcon} onPress={onGoBack}>
            <FontAwesome
              size={30}
              name="chevron-left"
              color={Colors.textColor.tiny}
            />
          </TouchableOpacity>
          <View style={styles.headerContainer}>
            <ProfileAvatar
              style={styles.profileAvatar}
              resizeMode="center"
              source={Images.logo}
              // editButton={renderEditAvatarButton}
            />
          </View>
          <View style={styles.formContainer}>
            <Input
              style={styles.formInput}
              status="control"
              autoCapitalize="none"
              placeholder="Email"
              icon={EmailIcon}
              value={email}
              onChangeText={setEmail}
            />
            <Input
              style={styles.formInput}
              status="control"
              autoCapitalize="none"
              placeholder="Số Điện Thoại"
              icon={PhoneIcon}
              value={phoneNumber}
              onChangeText={setPhoneNumber}
            />
            <Input
              style={styles.formInput}
              status="control"
              autoCapitalize="none"
              placeholder="Tên Tài Khoản"
              icon={PersonIcon}
              value={userName}
              onChangeText={setUserName}
            />
            <Input
              style={styles.formInput}
              status="control"
              autoCapitalize="none"
              secureTextEntry={!passwordVisible}
              placeholder="Mật Khẩu"
              icon={passwordVisible ? EyeIcon : EyeOffIcon}
              value={password}
              onChangeText={setPassword}
              onIconPress={onPasswordIconPress}
            />
            <Text style={{fontSize: 14, color: Colors.red}}>Lưu ý: Mật khẩu gồm chữ hoa, thường, số và kí tự </Text>
          </View>
          <Button
            style={styles.signUpButton}
            size="giant"
            onPress={onSignUpButtonPress}>
            ĐĂNG KÍ
          </Button>
          <Button
            style={styles.signInButton}
            appearance="ghost"
            status="control"
            onPress={onSignInButtonPress}>
           Đã có tài khoản? Đăng nhập
          </Button>
        </SafeAreaView>
      </ImageOverlay>
    </KeyboardAvoidingView>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUpScreen);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-1',
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 216,
    // backgroundColor: 'red',
  },
  backIcon: {
    position: 'absolute',
    left: '6%',
    top: '6%',
    backgroundColor: 'background-basic-color-1',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 999,
    zIndex: 1,
  },
  profileAvatar: {
    width: 116,
    height: 116,
    borderRadius: 58,
    alignSelf: 'center',
  },
  editAvatarButton: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  formContainer: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 16,
  },
  formInput: {
    marginTop: 16,
  },
  termsCheckBox: {
    marginTop: 24,
  },
  termsCheckBoxText: {
    color: 'text-control-color',
  },
  signUpButton: {
    marginVertical: 10,
    marginHorizontal: 16,
  },
  signInButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
});
