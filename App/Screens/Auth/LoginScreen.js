import React from 'react';
import { StyleSheet, View, AsyncStorage, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { Button, Input, Text } from '@ui-kitten/components';
import { ImageOverlay } from './extra/image-overlay.component';
import { EyeIcon, EyeOffIcon, PersonIcon } from './extra/icons';
import { KeyboardAvoidingView } from './extra/3rd-party';

import { SafeAreaLayout } from '../SafeAreaLayout';
import { LoadingIndicator } from '../LoadingIndicator';

import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import { Images } from '../../Themes';

const mapStateToProps = state => ({
  loginPayload: LoginSelectors.getPayload(state.login),
  loginFetching: LoginSelectors.getFetching(state.login),
  mePayload: MeSelectors.getPayload(state.me),
  meFetching: MeSelectors.getFetching(state.me),
});

const mapDispatchToProps = dispatch => {
  return {
    login: params => dispatch(LoginActions.loginRequest({ ...params })),
    getMe: params => dispatch(MeActions.meRequest({ ...params })),
  };
};

function LoginScreen(props) {
  const {
    navigation,
    login,
    meFetching,
    loginFetching,
    loginPayload,
    getMe,
    mePayload,
  } = props;

  const loadingIndicatorEnabled = meFetching || loginFetching;

  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  // const [saveAuthToken, setSaveAuthToken] = React.useState(false);
  const [passwordVisible, setPasswordVisible] = React.useState(false);


  const onSignInButtonPress = () => {
    login({
      username: username,
      password: password,
      isRemember: true,
    });
    // navigation && navigation.goBack();
  };

  const onSignUpButtonPress = () => {
    navigation && navigation.navigate('SignUp');
  };

  const onForgotPasswordButtonPress = () => {
    navigation && navigation.navigate('ForgotPassword');
  };

  const onPasswordIconPress = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <KeyboardAvoidingView>
      <LoadingIndicator enabled={loadingIndicatorEnabled} />
      <ImageOverlay
        style={styles.container}
        source={Images.loginBackground}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={styles.headerContainer}>
            <Text category="h1" status="control">
              Leecafeteria 4.0
            </Text>
            <Text style={styles.signInLabel} category="s1" status="control">
              Vui lòng đằng nhập tài khoản của bạn
            </Text>
          </View>
          <View style={styles.formContainer}>
            <Input
              status="control"
              placeholder="Tên Tài khoản"
              icon={PersonIcon}
              value={username}
              onChangeText={setUsername}
              returnKeyType="next"
            />
            <Input
              style={styles.passwordInput}
              status="control"
              placeholder="Mật khẩu"
              icon={passwordVisible ? EyeIcon : EyeOffIcon}
              value={password}
              secureTextEntry={!passwordVisible}
              onChangeText={setPassword}
              onIconPress={onPasswordIconPress}
            />
            <View style={styles.forgotPasswordContainer}>
              {/* <Button
                style={styles.forgotPasswordButton}
                appearance="ghost"
                status="control"
                onPress={onForgotPasswordButtonPress}>
                Forgot your password?
              </Button> */}
            </View>
          </View>
          <Button
            style={styles.signInButton}
            status="control"
            size="giant"
            onPress={onSignInButtonPress}>
            ĐĂNG NHẬP
          </Button>
          <Button
            style={styles.signUpButton}
            appearance="ghost"
            status="control"
            onPress={onSignUpButtonPress}>
            Chưa tạo tài khoản? Đăng kí
          </Button>
        </SafeAreaView>
      </ImageOverlay>
    </KeyboardAvoidingView>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 70,
    minHeight: 216,
  },
  formContainer: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 16,
  },
  signInLabel: {
    marginTop: 16,
  },
  signInButton: {
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
  forgotPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  passwordInput: {
    marginTop: 16,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
});
