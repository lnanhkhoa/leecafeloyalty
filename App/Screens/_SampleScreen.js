import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import { Images, ApplicationStyles, Colors, Fonts } from '../Themes/';

SamplScreen.propTypes = {};
export default SamplScreen;
function SamplScreen(props) {
  const { navigation } = props;
  return (
    <View style={styles.mainContainer}>
      <View style={styles.content}>
        <Text>SamplScreen</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
