import React from 'react';
import _ from 'lodash';
import {
  Dimensions,
  ImageBackground,
  ListRenderItemInfo,
  View,
} from 'react-native';
import {
  Button,
  Card,
  List,
  StyleService,
  Text,
  useStyleSheet,
} from '@ui-kitten/components';
import { connect } from 'react-redux';
import ProductActions, { ProductSelectors } from '../../Redux/ProductRedux';

const MenuFoodScreen = ({ navigation, route, productPayload }) => {
  const styles = useStyleSheet(themedStyles);
  const displayProducts = _.get(productPayload, 'data', []);
  const onItemPress = index => {
    navigation &&
      navigation.navigate('MenuFoodDetailScreen', {
        item: displayProducts[index],
      });
  };

  const renderItemHeader = item => (
    <ImageBackground
      style={styles.itemHeader}
      source={{ uri: item.thumbnail }}
    />
  );

  const renderProductItem = ({ item, index }) => (
    <Card
      style={styles.productItem}
      header={() => renderItemHeader(item)}
      onPress={() => onItemPress(index)}>
      <Text category="s1">{item.name}</Text>
      <Text appearance="hint" category="c1">
        {/* {item.price || '10.000'}đ */}
      </Text>
    </Card>
  );

  return (
    <List
      contentContainerStyle={styles.productList}
      data={displayProducts}
      numColumns={2}
      initialNumToRender={6}
      renderItem={renderProductItem}
    />
  );
};

MenuFoodScreen.propTypes = {};

const mapStateToProps = state => ({
  productPayload: ProductSelectors.getPayload(state.product),
});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuFoodScreen);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  productList: {
    paddingHorizontal: 8,
    paddingVertical: 16,
  },
  productItem: {
    flex: 1,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    backgroundColor: 'background-basic-color-1',
  },
  itemHeader: {
    height: 140,
  },
  itemFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconButton: {
    paddingHorizontal: 0,
  },
});
