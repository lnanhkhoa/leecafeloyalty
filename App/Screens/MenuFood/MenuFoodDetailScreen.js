import React from 'react';
import { ImageBackground, Platform, View } from 'react-native';
import {
  Layout,
  StyleService,
  Text,
  useStyleSheet,
} from '@ui-kitten/components';
import { KeyboardAvoidingView } from './extra/keyboard-avoiding-view.component';

import { HeaderBackButton } from '@react-navigation/stack';

const keyboardOffset = height =>
  Platform.select({
    android: 0,
    ios: height,
  });

export default ({ navigation, route }) => {
  const styles = useStyleSheet(themedStyles);
  const routeParams = route.params;
  const { item = {} } = routeParams;
  navigation.setOptions({
    title: item.name,
    headerLeft: () => (
      <HeaderBackButton onPress={() => navigation.goBack()} />
    ),
  });

  const renderHeader = () => (
    <Layout style={styles.header}>
      <ImageBackground
        style={styles.image}
        source={{ uri: item.thumbnail }}
      />
      <Layout style={styles.detailsContainer} level="1">
        <Text category="h6">{item.name}</Text>
        <Text style={styles.subtitle} appearance="hint" category="p2">
          {item.remark}
        </Text>
        <Text style={styles.price} category="h4">
          {/* {item.price}đ */}
        </Text>
        <Text style={styles.description} appearance="hint">
          {item.description}
        </Text>
      </Layout>
    </Layout>
  );

  return (
    <KeyboardAvoidingView style={styles.container} offset={keyboardOffset}>
      {renderHeader()}
    </KeyboardAvoidingView>
  );
};

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  commentList: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  header: {
    marginBottom: 8,
  },
  image: {
    height: 340,
    width: '100%',
  },
  detailsContainer: {
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  subtitle: {
    marginTop: 4,
  },
  price: {
    position: 'absolute',
    top: 24,
    right: 16,
  },
  description: {
    marginVertical: 16,
  },
  size: {
    marginBottom: 16,
  },
  colorGroup: {
    flexDirection: 'row',
    marginHorizontal: -8,
  },
  colorRadio: {
    marginHorizontal: 8,
  },
  actionContainer: {
    flexDirection: 'row',
    marginHorizontal: -8,
    marginTop: 24,
  },
  actionButton: {
    flex: 1,
    marginHorizontal: 8,
  },
  sectionLabel: {
    marginVertical: 8,
  },
  commentInputLabel: {
    fontSize: 16,
    marginBottom: 8,
    color: 'text-basic-color',
  },
  commentInput: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
});
