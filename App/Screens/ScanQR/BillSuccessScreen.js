import React, { memo, useState } from 'react';
import moment from 'moment';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
// components
import { Images, Colors, ApplicationStyles, Fonts } from '../../Themes';

const BillSuccessScreen = props => {
  const { navigation } = props;

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <View style={styles.imageBox}>
          <Images.IllustrationConfirmation {...styles.image} />
        </View>
        <View style={styles.textBox}>
          <Text style={styles.textBold}>Giao dịch thành công</Text>
        </View>
      </View>
      <View style={styles.button}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home', { screen: 'Home' })}
          style={{
            backgroundColor: Colors.greenMedium,
            padding: 20,
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <Text
            style={{
              ...Fonts.style.h3,
              fontWeight: 'bold',
              color: Colors.white,
            }}>
            Về trang chính
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default memo(BillSuccessScreen);

const styles = StyleSheet.create({
  buttonBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 28,
  },
  imageBox: {
    paddingTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 200,
    width: 200,
  },
  textBox: {
    paddingTop: 28,
    paddingHorizontal: 40,
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    ...Fonts.style.bodyRegular,
    color: Colors.gray2,
    lineHeight: 30,
  },
  textBold: {
    textAlign: 'center',
    ...Fonts.style.subtitleSemibold,
    color: Colors.gray2,
    lineHeight: 30,
  },
  button: {
    paddingHorizontal: 25,
    paddingBottom: 50,
  },
});
