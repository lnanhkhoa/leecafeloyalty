import React, { useState, useEffect, memo } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import { Fonts, Colors } from '../../Themes';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import LeePayActions, { LeePaySelectors } from '../../Redux/LeePayRedux';

const BillDetailScreen = props => {
  const {
    navigation,
    route,
    leePayRequest,
    leePayPayload,
    meData,
    mePayload,
  } = props;

  navigation.setOptions({
    title: 'Thanh toán',
    headerLeft: () => <HeaderBackButton onPress={() => navigation.goBack()} />,
  });

  const routeParams = _.get(route, 'params', {});
  const { a, b, payload } = routeParams;
  const data = _.split(b, '-');
  const ProductName = _.last(data);
  const [MachineCode, OrderNo, ProductCode] = _.split(_.head(data), '_');

  const [isPressConfirm, setisPressConfirm] = useState(false);

  const Authorization = _.get(meData, 'Authorization');
  const userId = _.get(mePayload, 'id');

  useEffect(() => {
    if (isPressConfirm && leePayPayload) {
      gotoSuccessScreen();
    }
    return () => {};
  }, [leePayPayload]);

  const gotoSuccessScreen = () => navigation.navigate('BillSuccessScreen');
  const onPressConfirm = () => {
    setisPressConfirm(true);
    leePayRequest({
      // userId,
      // payload,
      Authorization,
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <Text
          style={{
            ...Fonts.style.h3,
            fontWeight: 'normal',
            color: Colors.black,
            marginTop: 50,
            marginBottom: 20,
            paddingHorizontal: 25,
          }}>
          Chi tiết giao dịch
        </Text>
        <View style={styles.box}>
          <View style={styles.item}>
            <Text>Số hoá đơn</Text>
            <Text style={{ color: Colors.blueOcean }}>{OrderNo}</Text>
          </View>
          <View style={styles.item}>
            <Text>Tên sản phẩm</Text>
            <Text style={{ color: Colors.blueOcean }}>{ProductName}</Text>
          </View>
          <View style={styles.item}>
            <Text>Mã sản phẩm</Text>
            <Text style={{ color: Colors.blueOcean }}>{ProductCode}</Text>
          </View>
          <View style={styles.item}>
            <Text>Mã máy bán hàng</Text>
            <Text style={{ color: Colors.blueOcean }}>{MachineCode}</Text>
          </View>
          <View style={[styles.item, { borderTopWidth: 0.25 }]}>
            <Text
              style={{
                ...Fonts.style.h4,
                fontWeight: 'bold',
                paddingTop: 10,
              }}>
              Phí
            </Text>
            <Text>0</Text>
          </View>
          <View
            style={[styles.item, { borderTopWidth: 0.25, marginVertical: 10 }]}>
            <Text
              style={{
                ...Fonts.style.h4,
                fontWeight: 'bold',
                paddingTop: 10,
              }}>
              Số point cần thanh toán
            </Text>
            <Text style={{ ...Fonts.style.h3, fontWeight: 'bold' }}>{a}</Text>
          </View>
        </View>
      </View>
      <View style={styles.buttonBox}>
        <TouchableOpacity
          onPress={onPressConfirm}
          style={{
            backgroundColor: Colors.greenMedium,
            padding: 20,
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <Text
            style={{
              ...Fonts.style.h3,
              fontWeight: 'bold',
              color: Colors.white,
            }}>
            Xác Nhận
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = state => ({
  mePayload: MeSelectors.getPayload(state.me),
  meData: MeSelectors.getData(state.me),
  leePayPayload: LeePaySelectors.getPayload(state.leepay),
});

const mapDispatchToProps = dispatch => {
  return {
    leePayRequest: params => dispatch(LeePayActions.leePayRequest(params)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BillDetailScreen);

const styles = StyleSheet.create({
  box: {
    backgroundColor: 'white',
    paddingVertical: 20,
  },
  item: {
    marginHorizontal: 25,
    marginVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttonBox: {
    paddingHorizontal: 25,
    marginVertical: 30,
  },
});
