import React, { useState, useEffect } from 'react';
import qs from 'qs';
import {
  View,
  Text,
  StyleSheet,
  Buttonm,
  useWindowDimensions,
} from 'react-native';
import _ from 'lodash';
import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { TouchableOpacity } from 'react-native-gesture-handler';

function ScanQRScreen(props) {
  const { navigation, route } = props;
  const { width, height } = useWindowDimensions();

  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [focusCamera, setFocusCamera] = useState(true);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
    navigation.addListener('focus', () => setFocusCamera(true));
    navigation.addListener('blur', () => {
      setFocusCamera(false);
      setScanned(false);
    });
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    const stringDataArray = _.split(data, '?');
    const lastStringData = qs.parse(_.last(stringDataArray));
    if (_.has(lastStringData, ['a']) && _.has(lastStringData, ['b']))
      navigation.push('BillDetailScreen', {
        ...lastStringData,
        payload: data,
      });
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
      }}>
      {focusCamera && (
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}>
          <View style={{ flex: 1 }}>
            <View style={styles.opacityView} />
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
              }}>
              <View style={styles.opacityView} />
              <View
                style={{
                  borderColor: 'white',
                  borderWidth: 3,
                  borderRadius: 5,
                  width: height / 3,
                  backgroundColor: 'transparent',
                }}
              />
              <View style={styles.opacityView} />
            </View>
            <View
              style={[
                styles.opacityView,
                { alignItems: 'center', justifyContent: 'center' },
              ]}>
              <TouchableOpacity onPress={() => setScanned(false)}>
                <Text style={{ ...Fonts.style.h3, color: Colors.white }}>
                  Thử lại
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </BarCodeScanner>
      )}
    </View>
  );
}

export default ScanQRScreen;

const styles = StyleSheet.create({
  opacityView: {
    flex: 1,
    backgroundColor: Colors.grayDark,
    opacity: 0.95,
  },
});
