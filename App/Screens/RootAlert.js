import _ from 'lodash';
import React, { Component, memo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Alert, View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Metrics, Colors, Fonts } from '../Themes';
import RootAlertActions, { RootAlertSelectors } from '../Redux/RootAlertRedux';


const { width, height } = Dimensions.get('screen');

function RootAlert(props) {
  const { alertPayload, rootAlertHide } = props;
  const { show, title, description } = alertPayload;

  const onPressAlert = () => { rootAlertHide() }

  
  if (!show) return null;
  return (
    <View style={styles.container}>
      <View style={styles.shadow} />
      <View style={styles.content}>
        <View style={{ flex: 1, paddingVertical: 10 }}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.description}>{description}</Text>
        </View>
        <TouchableOpacity style={styles.button} onPress={onPressAlert}>
          <Text style={{ color: Colors.blueOcean, ...Fonts.style.h4, fontWeight: "bold"}}>OK</Text>
        </TouchableOpacity>
      </View>
    </View>
  );


}

RootAlert.propTypes = {};

const mapStateToProps = state => ({
  alertPayload: state.saga.alert
});

const mapDispatchToProps = dispatch => {
  return {
    rootAlertHide: (params) => dispatch(RootAlertActions.rootAlertHide(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootAlert);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: width,
    height: height
  },
  shadow: {
    width: width,
    height: height,
    opacity: 0.7,
    backgroundColor: Colors.grayLight,
  },
  content: {
    position: 'absolute',
    marginHorizontal: 10,
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  title: {
    ...Fonts.style.h3,
    fontFamily: Fonts.type.semiBold,
    textAlign: 'center',
    padding: 5
  },
  description: {
    ...Fonts.style.h5,
    textAlign: 'center'
  },
  button: {
    borderTopWidth: 0.25,
    width: '100%',
    paddingVertical: 5,
    alignItems: 'center',
  },


});
