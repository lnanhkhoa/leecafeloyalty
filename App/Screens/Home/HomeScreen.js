import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import ProfileItem from '../../Components/ProfileItem';
import MenuContent from '../../Components/MenuContent';
import NewsItem2 from '../../Components/NewsItem2';
import { LinearGradient } from 'expo-linear-gradient';
import { Images, ApplicationStyles, Colors, Fonts } from '../../Themes/';

// redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import NewsActions, { NewsSelectors } from '../../Redux/NewsRedux';
const { width: WIDTH } = Dimensions.get('screen');

function HomeScreen(props) {
  const { navigation, newsPayload = {} } = props;
  const mePayload = _.get(props, 'mePayload') || {};
  const point = _.get(mePayload, 'point', 0);
  const dataNews = _.get(newsPayload, 'data', []);
  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ProfileItem  navigation={navigation} />
          <ScrollView style={styles.content}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('HistoryPaymentScreen');
              }}>
              <LinearGradient
                colors={[Colors.linear.start, Colors.linear.end]}
                style={{
                  padding: 30,
                  alignItems: 'center',
                  marginVertical: 5,
                }}>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      ...Fonts.style.h2,
                      color: Colors.textColor.black,
                    }}>
                    Số dư tài khoản
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 20,
                    }}>
                    <Text
                      style={{
                        ...Fonts.style.h2,
                        color: Colors.textColor.black,
                      }}>
                      {point}
                    </Text>
                    <Image
                      source={Images.coffeeBean}
                      style={{ width: 25, height: 25, marginHorizontal: 5 }}
                    />
                  </View>
                </View>
              </LinearGradient>
            </TouchableOpacity>
            <View style={styles.menu}>
              <MenuContent navigation={navigation} />
            </View>
            <View style={styles.news}>
              <FlatList
                scrollEnabled={false}
                data={dataNews}
                keyExtractor={(item, index) => item.id}
                ListHeaderComponentStyle={{ padding: 10 }}
                renderItem={({ item }) => (
                  <NewsItem2 item={item} navigation={navigation} />
                )}
              />
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
}

HomeScreen.propTypes = {};

const mapStateToProps = state => ({
  mePayload: MeSelectors.getPayload(state.me),
  newsPayload: NewsSelectors.getPayload(state.news),
});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.block,
    // ...ApplicationStyles.specialApp.shadow,
  },
  menu: {
    backgroundColor: 'white',
    marginVertical: 5,
  },
  news: {
    // height: 200,
    // backgroundColor: 'blue',
    marginVertical: 5,
  },

  newsTitle: {
    ...Fonts.style.h3,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  foodMenu: {
    height: 200,
    backgroundColor: 'blue',
    marginVertical: 5,
  },
});
