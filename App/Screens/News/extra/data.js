const typeTransaction = {
  WithDraw: 0,
  TopUp: 1,
  Payment: 2,
  Reward: 3,
  Refund: 4,
  Transfer: 5,
};

const data = [
  {
    id: '1',
    title: 'QUYỀN LỢI KHI HỢP TÁC LEE CAFETERIA 4.0',
    image: 'http://leecafeteria.com/wp-content/uploads/2019/11/6.png',
    date: '06 Aug, 2019',
    htmlContent: `
    <div class="benefit__inner wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 1s; animation-name: zoomIn;">
    <div class="benefit__content">
        <div class="section__tite text-center">
    </div>
    <p><span style="font-size: 20px;"><img class="alignnone wp-image-276" src="http://leecafeteria.com/wp-content/uploads/2019/11/tay-chỉ.png" alt="Tay Chỉ" width="27" height="21"> Được hỗ trợ tư vấn và thuê mặt bằng để đạt doanh thu cao nhất</span></p>
    <p><span style="font-size: 20px;"><strong><img class="alignnone wp-image-276" src="http://leecafeteria.com/wp-content/uploads/2019/11/tay-chỉ.png" alt="Tay Chỉ" width="27" height="21"></strong>Được chuyển giao công nghệ từ Lee Cafeteria 4.0</span></p>
    <p><span style="font-size: 20px;"><img class="alignnone wp-image-276" src="http://leecafeteria.com/wp-content/uploads/2019/11/tay-chỉ.png" alt="Tay Chỉ" width="27" height="21"> Được cung cấp nguyên liệu sạch, chuẩn VSATTP</span></p>
    <p><span style="font-size: 20px;"><img class="alignnone wp-image-276" src="http://leecafeteria.com/wp-content/uploads/2019/11/tay-chỉ.png" alt="Tay Chỉ" width="27" height="21"> Hỗ trợ toàn diện về chiến lược marketing, quảng bá để tặng doanh thu, lợi nhuận</span></p>
    <p><span style="font-size: 20px;"><img class="alignnone wp-image-276" src="http://leecafeteria.com/wp-content/uploads/2019/11/tay-chỉ.png" alt="Tay Chỉ" width="27" height="21"> Dịch vụ bảo hiểm, bảo trì máy chuyên nghiệp</span></p>
    <p><span style="font-size: 20px;"><img class="alignnone wp-image-276" src="http://leecafeteria.com/wp-content/uploads/2019/11/tay-chỉ.png" alt="Tay Chỉ" width="27" height="21"> Đồng hành với nhà đầu tư 24/24 trong suốt quá trình kinh doanh tại Lee Cafeteria 4.0</span></p>
        </div>
    </div>
    `,
  },
  {
    id: '2',
    title: 'LÝ DO CHỌN LEE CAFETERIA 4.0',
    image:
      'http://192.168.0.108:1337/uploads/Screenshot_from_2020-04-27_11-27-55_5a1839d2ac.png',
    date: '06 Aug, 2019',
    htmlContent: `
    <section id="why_me" class="why_me wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
    <div class="container">
        <div class="row">
          <div class="col-6 col-md-4">
            <div class="why_me__item text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 1s; animation-name: zoomIn;">
                <h6 class="title">MÔ HÌNH CHUYÊN NGHIỆP</h6>
                <p>Quy trình chuyển giao bài bản, không cần biết pha chế, nguồn nguyên liệu sạch. Marketing theo hệ thống.</p>
            </div>
        </div>
        <!-- end column -->
        <div class="col-6 col-md-4">
            <div class="why_me__item text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1.3s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.3s; animation-name: zoomIn;">
                <h6 class="title">THU HỒI VỐN NHANH</h6>
                <p>Tỉ suất lợi nhuận cao. Tối giản chi phí, tối đa hoá lợi nhuận. Thu hồi vốn nhanh từ 6 - 8 tháng</p>
            </div>
        </div>
        <!-- end column -->
        <div class="col-6 col-md-4">
            <div class="why_me__item text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1.5" style="visibility: visible; animation-duration: 1s; animation-name: zoomIn;">
                <h6 class="title">CHI PHÍ</h6>
                <p>Chi phí nhượng quyền thấp và đa dạng giải pháp đầu tư, chỉ từ 80 triệu đồng. Nhiều phương án phù hợp với tài chính của các nhà đầu tư.</p>
            </div>
        </div>
        <!-- end column -->
        <div class="col-6 col-md-4">
            <div class="why_me__item text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1.7s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.7s; animation-name: zoomIn;">
                <h6 class="title">MENU THỨC UỐNG ĐA DẠNG</h6>
                <p>Menu hơn 16 loại thức uống: cafe, trà sữa, cacao, matcha, siro,... Thức uống sạch, thơm ngon.</p>
            </div>
        </div>
        <!-- end column -->
        <div class="col-6 col-md-4">
            <div class="why_me__item text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.9s; animation-name: zoomIn;">
                <h6 class="title">QUẢN LÝ DOANH THU TỰ ĐỘNG</h6>
                <p>Thanh toán tiện lợi bằng tiền mặt, ví điện tử.
Quản lí doanh thu qua App trên điện thoại.</p>
            </div>
        </div>
        <!-- end column -->
        <div class="col-6 col-md-4">
            <div class="why_me__item text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="2.1s" style="visibility: visible; animation-duration: 1s; animation-delay: 2.1s; animation-name: zoomIn;">
                <h6 class="title">GIÁ ĐỒ UỐNG</h6>
                <p>Giá phù hợp túi tiền HSSV.
Điều chỉnh giá phù hợp theo địa điểm đặt máy.</p>
            </div>
        </div>
                                
        </div>
    </div>
</section>`,
  },

  {
    id: '3',
    title: 'Trưng bày sản phẩm thực tế tại triển lãm',
    image:
      'http://leecafeteria.com/wp-content/uploads/2019/10/f2c0effa1147f619af56-300x225.jpg',
    htmlContent:
      '<div class="single__content"><p>Sản phẩm máy pha chế cafe tự động lần đầu tiên được giới thiệu tại triển lãm. Sản phẩm được đông đảo người sử dụng dùng thử và hưởng ứng.</p><p> <img class="alignnone size-medium wp-image-267" src="http://leecafeteria.com/wp-content/uploads/2019/10/ec389b0d65b082eedba1-225x300.jpg" alt="Ec389b0d65b082eedba1" width="225" height="300" srcset="http://leecafeteria.com/wp-content/uploads/2019/10/ec389b0d65b082eedba1-225x300.jpg 225w, http://leecafeteria.com/wp-content/uploads/2019/10/ec389b0d65b082eedba1-768x1024.jpg 768w, http://leecafeteria.com/wp-content/uploads/2019/10/ec389b0d65b082eedba1.jpg 960w" sizes="(max-width: 225px) 100vw, 225px"> <img class="alignnone size-medium wp-image-266" src="http://leecafeteria.com/wp-content/uploads/2019/10/dca5889f7622917cc833-225x300.jpg" alt="Dca5889f7622917cc833" width="225" height="300" srcset="http://leecafeteria.com/wp-content/uploads/2019/10/dca5889f7622917cc833-225x300.jpg 225w, http://leecafeteria.com/wp-content/uploads/2019/10/dca5889f7622917cc833-768x1024.jpg 768w" sizes="(max-width: 225px) 100vw, 225px"> <img class="alignnone size-medium wp-image-262" src="http://leecafeteria.com/wp-content/uploads/2019/10/b301c2f63b4bdc15855a-225x300.jpg" alt="B301c2f63b4bdc15855a" width="225" height="300" srcset="http://leecafeteria.com/wp-content/uploads/2019/10/b301c2f63b4bdc15855a-225x300.jpg 225w, http://leecafeteria.com/wp-content/uploads/2019/10/b301c2f63b4bdc15855a-768x1024.jpg 768w" sizes="(max-width: 225px) 100vw, 225px"> <img class="alignnone size-medium wp-image-261" src="http://leecafeteria.com/wp-content/uploads/2019/10/af2b8d1e73a394fdcdb2-225x300.jpg" alt="Af2b8d1e73a394fdcdb2" width="225" height="300" srcset="http://leecafeteria.com/wp-content/uploads/2019/10/af2b8d1e73a394fdcdb2-225x300.jpg 225w, http://leecafeteria.com/wp-content/uploads/2019/10/af2b8d1e73a394fdcdb2-768x1024.jpg 768w" sizes="(max-width: 225px) 100vw, 225px"><img class="alignnone size-medium wp-image-268" src="http://leecafeteria.com/wp-content/uploads/2019/10/f2c0effa1147f619af56-300x225.jpg" alt="F2c0effa1147f619af56" width="300" height="225" srcset="http://leecafeteria.com/wp-content/uploads/2019/10/f2c0effa1147f619af56-300x225.jpg 300w, http://leecafeteria.com/wp-content/uploads/2019/10/f2c0effa1147f619af56-768x576.jpg 768w, http://leecafeteria.com/wp-content/uploads/2019/10/f2c0effa1147f619af56-1024x768.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px"></p></div>',
  },
];
export default data;
