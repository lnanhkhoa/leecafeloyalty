import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  ScrollView,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { WebView } from 'react-native-webview';

import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import { Colors, Fonts } from '../../Themes';
const NewsDetailScreen = props => {
  const { navigation, route } = props;
  const routeParams = route.params || {};
  // const { id } = routeParams;

  const {
    id = '1',
    title,
    thumbnail,
    date = '06 Aug, 2019',
    htmlContent,
  } = routeParams;

  navigation.setOptions({
    title: title,
    headerLeft: () => <HeaderBackButton onPress={() => navigation.goBack()} />,
  });

  const runFirst = `
      window.isNativeApp = true;
      true; // note: this is required, or you'll sometimes get silent failures
    `;

  const htmlContentContainer = `
   <html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>
  <h4 style="text-align: center;">${title}</h4>
  <img style="alignSelf: center;" src="${thumbnail}" alt="F2c0effa1147f619af56" width="100%"/> 
    ${htmlContent}
     </body></html>
  `;

  const [webViewHeight, setwebViewHeight] = useState(200);
  const onWebViewMessage = event => {
    setwebViewHeight(Number(event.nativeEvent.data));
  };

  if (!id) return <View style={{ flex: 1 }} />;
  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <WebView
        javaScriptEnabled
        hideKeyboardAccessoryView={true}
        injectedJavaScriptBeforeContentLoaded={runFirst}
        style={{ flex: 1, width: '100%' }}
        automaticallyAdjustContentInsets={false}
        cacheEnabled={true}
        scalesPageToFit={true}
        source={{ html: htmlContentContainer }}
      />
    </View>
  );
};

export default NewsDetailScreen;

const styles = StyleSheet.create({
  header: {},
  body: { flex: 1 },
});
