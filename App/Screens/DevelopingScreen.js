import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';
import { Images, Colors } from '../Themes';

import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';

const DevelopingScreen = ({ navigation }) => {
  navigation.setOptions({
    title: "Tính năng đang phát triển",
    headerLeft: () => <HeaderBackButton onPress={() => navigation.goBack()} />,
  });
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Image
        source={Images.developingBG}
        style={{
          width: '95%',
          height: 300,
          marginTop: -60,
          alignSelf: 'center',
        }}
      />
      <Text
        style={{
          fontSize: 16,
          color: Colors.black,
          paddingVertical: 20,
          width: '90%',
          alignSelf: 'center',
          textAlign: 'center',
        }}>
        Tính năng này đang trong quá trình phát triển
      </Text>
    </View>
  );
};

export default DevelopingScreen;

const styles = StyleSheet.create({});
