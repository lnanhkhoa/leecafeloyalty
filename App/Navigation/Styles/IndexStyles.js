import { StyleSheet, Dimensions } from 'react-native';
import { Colors, Metrics, Fonts } from '../../Themes';
const { width } = Dimensions.get('screen');

export default StyleSheet.create({
  back_btn: {
    paddingLeft: (5 / 375) * width,
    paddingBottom: Metrics.paddingXTiny,
  },
  back_btn_border: {
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
  },
  tab_content: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon_tab: {
    fontSize: Fonts.size.textXHuge,
    marginBottom: Metrics.marginXTiny,
  },
  // Text
  header_title_text: {
    fontSize: (16 / 375) * Metrics.width,
    fontWeight: 'bold',
    textAlign: 'center',
    flex: 1,
  },
  textBottomTab: {
    fontSize: Fonts.size.textSmall,
    fontFamily: Fonts.type.fontRegular,
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
});
