import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import StackDefaultProps from './StackDefaultProps';
import SettingScreen from '../Screens/Other/SettingScreen';
import MemberInfoScreen from '../Screens/Other/MemberInfoScreen';
import HistoryPaymentScreen from '../Screens/Other/HistoryPaymentScreen';
import AboutMeScreen from '../Screens/Other/AboutMeScreen';
import ProfileDetailScreen from '../Screens/Other/ProfileDetailScreen';


const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator headerMode="screen">
      <Stack.Screen
        name="SettingScreen"
        component={SettingScreen}
        options={{
          header: props => null,
        }}
      />
    </Stack.Navigator>
  );
};
