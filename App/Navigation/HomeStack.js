import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../Screens/Home/HomeScreen';
import NewsDetailScreen from '../Screens/News/NewsDetailScreen';
import StackDefaultProps from './StackDefaultProps';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator {...StackDefaultProps} initialRouteName="HomeScreen">
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
    </Stack.Navigator>
  );
};
