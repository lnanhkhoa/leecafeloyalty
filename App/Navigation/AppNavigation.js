import React from 'react';
import {
  Platform,
  Text,
  Button,
  TouchableOpacity,
  Dimensions,
  View,
  Image,
} from 'react-native';
import { AntDesign, Feather, MaterialCommunityIcons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { LinearGradient } from 'expo-linear-gradient';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

import NewsDetailScreen from '../Screens/News/NewsDetailScreen';
import DevelopingScreen from '../Screens/DevelopingScreen';
import LockDetailScreen from '../Screens/Lock/LockDetailScreen';
import MenuFoodDetailScreen from '../Screens/MenuFood/MenuFoodDetailScreen';
import MyCouponScreen from '../Screens/Home/MyCouponScreen';
import BillDetailScreen from '../Screens/ScanQR/BillDetailScreen';
import BillSuccessScreen from '../Screens/ScanQR/BillSuccessScreen';
import HistoryPaymentScreen from '../Screens/Other/HistoryPaymentScreen';

import MemberInfoScreen from '../Screens/Other/MemberInfoScreen';
import AboutMeScreen from '../Screens/Other/AboutMeScreen';
import ProfileDetailScreen from '../Screens/Other/ProfileDetailScreen';

// COMPONENTS
import { Colors, Images, Fonts, Metrics } from '../Themes';

import HomeStack from './HomeStack';
import LockStack from './LockStack';
import MenuFoodStack from './MenuFoodStack';
import ScanQRStack from './ScanQRStack';
import OtherStack from './OtherStack';
import TabBarIcon from './TabBarIcon';
import StackDefaultProps from './StackDefaultProps';

const BottomRouter = () => {
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      lazy={true}
      tabBarOptions={{
        activeTintColor: Colors.yellow,
      }}>
      <BottomTab.Screen
        name="Home"
        component={HomeStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="ios-home" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Lock"
        component={LockStack}
        options={{
          title: 'Điểm bán hàng',
          tabBarIcon: ({ focused }) => (
            <Feather
              name="navigation"
              size={27}
              style={{ marginBottom: -3 }}
              color={focused ? Colors.main : Colors.steel}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="ScanQR"
        component={ScanQRStack}
        options={{
          title: '',
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 28,
                overflow: 'hidden',
              }}>
              <LinearGradient
                colors={[Colors.linearButton.start, Colors.linearButton.end]}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 8,
                    width: 56,
                    height: 56,
                  }}>
                  <AntDesign
                    name="scan1"
                    size={35}
                    style={{ paddingTop: 2, paddingBottom: 1 }}
                    color={Colors.textColor.black}
                  />
                </View>
              </LinearGradient>
            </View>
          ),
        }}
      />
      <BottomTab.Screen
        name="History"
        component={MenuFoodStack}
        options={{
          title: 'Menu',
          tabBarIcon: ({ focused }) => (
            <MaterialCommunityIcons
              name="food"
              size={30}
              color={focused ? Colors.main : Colors.steel}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Other"
        component={OtherStack}
        options={{
          title: 'Tài khoản',
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="md-person" />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default () => (
  <Stack.Navigator initialRouteName="Home" headerMode="float">
    <Stack.Screen
      name="Home"
      component={BottomRouter}
      options={{ header: () => null }}
    />
    {/*  */}
    {/*  */}
    {/*  */}
    <Stack.Screen
      name="NewsDetailScreen"
      component={NewsDetailScreen}
      options={{
        title: 'Tin tức',
        headerTitleAlign: 'center',
        headerTitleContainerStyle: {
          width: '50%',
        },
        headerBackAllowFontScaling: true,
      }}
    />
    <Stack.Screen name="LockDetailScreen" component={LockDetailScreen} />
    <Stack.Screen
      name="MenuFoodDetailScreen"
      component={MenuFoodDetailScreen}
    />
    <Stack.Screen name="DevelopingScreen" component={DevelopingScreen} />
    <Stack.Screen
      name="MyCouponScreen"
      component={MyCouponScreen}
      options={{ title: 'Ưu Đãi của bạn' }}
    />
    <Stack.Screen name="BillDetailScreen" component={BillDetailScreen} />
    <Stack.Screen
      name="BillSuccessScreen"
      component={BillSuccessScreen}
      options={{
        title: 'Thanh toán thành công',
        headerLeft: () => null,
      }}
    />
    <Stack.Screen
      name="HistoryPaymentScreen"
      component={HistoryPaymentScreen}
      options={{
        title: 'Lịch sử giao dịch',
      }}
    />
    <Stack.Screen name="MemberInfoScreen" component={MemberInfoScreen} />
    <Stack.Screen
      name="AboutMeScreen"
      component={AboutMeScreen}
      options={{
        title: 'Thông tin liên hệ',
      }}
    />
    <Stack.Screen
      name="ProfileDetailScreen"
      component={ProfileDetailScreen}
      options={{
        title: 'Thông tin tài khoản',
      }}
    />
  </Stack.Navigator>
);
