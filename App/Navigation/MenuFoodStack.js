import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import MenuFoodScreen from '../Screens/MenuFood/MenuFoodScreen';
import StackDefaultProps from './StackDefaultProps';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator {...StackDefaultProps} initialRouteName="MenuFoodScreen">
      <Stack.Screen name="MenuFoodScreen" component={MenuFoodScreen} />
    </Stack.Navigator>
  );
};
