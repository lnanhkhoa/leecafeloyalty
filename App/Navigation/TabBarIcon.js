import * as React from 'react';
import { Ionicons } from '@expo/vector-icons';

import { Colors } from '../Themes';

export default function TabBarIcon(props) {
  return (
    <Ionicons
      name={props.name}
      size={30}
      style={{ marginBottom: -3 }}
      color={props.focused ? Colors.main : Colors.steel}
    />
  );
}
