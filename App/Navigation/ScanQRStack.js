import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ScanQRScreen from '../Screens/ScanQR/ScanQRScreen';
import StackDefaultProps from './StackDefaultProps';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator initialRouteName="ScanQRScreen" headerMode="none">
      <Stack.Screen name="ScanQRScreen" component={ScanQRScreen} />
    </Stack.Navigator>
  );
};
