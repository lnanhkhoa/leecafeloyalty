import React from 'react';
import {
  Platform,
  Text,
  Button,
  TouchableOpacity,
  Dimensions,
  View,
  Image,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from '../Screens/Auth/SplashScreen';
import OnboardingScreen from '../Screens/Auth/OnboardingScreen';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator initialRouteName="Splash" headerMode="none">
      <Stack.Screen name="Splash" component={SplashScreen} />
    </Stack.Navigator>
  );
};
