import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import NewsScreen from '../Screens/News/NewsScreen';
import StackDefaultProps from './StackDefaultProps';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator initialRouteName="NewsScreen" headerMode="none">
      <Stack.Screen name="NewsScreen" component={NewsScreen} />
    </Stack.Navigator>
  );
};
