import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import LockScreen from '../Screens/Lock/LockScreen';
import StackDefaultProps from './StackDefaultProps';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator initialRouteName="LockScreen" headerMode="none">
      <Stack.Screen name="LockScreen" component={LockScreen} />
    </Stack.Navigator>
  );
};
