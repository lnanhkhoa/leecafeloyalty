import React from 'react';
import {
  Platform,
  Text,
  Button,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  View,
  Image,
} from 'react-native';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';

import LoginScreen from '../Screens/Auth/LoginScreen';
import SignUpScreen from '../Screens/Auth/SignUpScreen';
import OnboardingScreen from '../Screens/Auth/OnboardingScreen';
import ForgotPasswordScreen from '../Screens/Auth/ForgotPasswordScreen';

const Stack = createStackNavigator();

export default (props) => {
  return (
    <Stack.Navigator initialRouteName="Onboarding" headerMode="none" {...props}>
      <Stack.Screen name="Onboarding" component={OnboardingScreen} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="SignUp" component={SignUpScreen} />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
    </Stack.Navigator>
  );
};
