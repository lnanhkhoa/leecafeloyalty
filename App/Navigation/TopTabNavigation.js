import React from 'react';
import { View, Text } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

// import CouponScreen from '../Screens/Coupon/CouponScreen';

function HomeScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Home!</Text>
    </View>
  );
}

const Tab = createMaterialTopTabNavigator();
export default function MyTabs(props) {
  const { Tab1 = Function, Tab2 = Function } = props;
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Tab1} />
      <Tab.Screen name="Settings" component={Tab2} />
    </Tab.Navigator>
  );
}
