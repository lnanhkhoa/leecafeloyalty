/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the Infinite Red Slack channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/
import _ from 'lodash';
import { call, put } from 'redux-saga/effects';
import RootAlertActions from '../Redux/RootAlertRedux';

export function* showRootAlert(action) {
  const { payload } = action;
  const descriptions = _.map(payload.errors, error => error.description);
  yield put(RootAlertActions.rootAlertShow({
    description: _.join(descriptions, '\n')
  }));
}
