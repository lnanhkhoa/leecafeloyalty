import { takeLatest, all } from 'redux-saga/effects';
import API from '../Services/Api';
import FixtureAPI from '../Services/FixtureApi';
import DebugConfig from '../Config/DebugConfig';

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux';
import { GithubTypes } from '../Redux/GithubRedux';
import { MeTypes } from '../Redux/MeRedux';
import { LoginTypes } from '../Redux/LoginRedux';
import { NewsTypes } from '../Redux/NewsRedux';
import { SignUpTypes } from '../Redux/SignUpRedux';
import { ProductTypes } from '../Redux/ProductRedux';
import { MachineTypes } from '../Redux/MachineRedux';
import { TransactionTypes } from '../Redux/TransactionRedux';
import { LeePayTypes } from '../Redux/LeePayRedux';
/* ------------- Sagas ------------- */

import { startup } from './StartupSagas';
import { getMe } from './MeSagas';
import { postlogin, getSignOut } from './LoginSagas';
import { getNews } from './NewsSagas';
import { getProduct } from './ProductSagas';
import { postSignUp } from './SignUpSagas';
import { getUserAvatar } from './GithubSagas';
import { getMachine } from './MachineSagas';
import { postLeePayConfirm } from './LeePaySagas';
import { postTransaction } from './TransactionSagas';
import { showRootAlert } from './RootAlertSagas';

const api = DebugConfig.useFixtures ? FixtureAPI : API.create();

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),
    takeLatest(MeTypes.ME_REQUEST, getMe, api),
    takeLatest(LoginTypes.LOGIN_REQUEST, postlogin, api),
    takeLatest(SignUpTypes.SIGN_UP_REQUEST, postSignUp, api),
    takeLatest(LoginTypes.SIGN_OUT, getSignOut, api),
    takeLatest(NewsTypes.NEWS_REQUEST, getNews, api),
    takeLatest(ProductTypes.PRODUCT_REQUEST, getProduct, api),
    takeLatest(MachineTypes.MACHINE_REQUEST, getMachine, api),
    takeLatest(TransactionTypes.TRANSACTION_REQUEST, postTransaction, api),
    takeLatest(LeePayTypes.LEE_PAY_REQUEST, postLeePayConfirm, api),
  
    // Alert api errors
    takeLatest(SignUpTypes.SIGN_UP_FAILURE, showRootAlert),
  ]);
}
