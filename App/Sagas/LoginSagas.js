/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the Infinite Red Slack channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import { call, put } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import LoginActions, { LoginSelectors } from '../Redux/LoginRedux';
import MeActions, { MeSelectors } from '../Redux/MeRedux';
import RootAlertActions from '../Redux/RootAlertRedux';

export function* postlogin(api, action) {
  const { data } = action;
  const response = yield call(api.postlogin, data);

  if (response.ok && response.data.succeeded) {
    yield put(LoginActions.loginSuccess(response.data));
    yield put(MeActions.meRequest({ Authorization: response.data.jwtToken }));
    AsyncStorage.setItem('Authorization', response.data.jwtToken);
  } else {
    yield put(LoginActions.loginFailure());
    yield put(
      RootAlertActions.rootAlertShow(
        response.data
          ? response.data.errors
          : [{ description: response.problem }],
      ),
    );
  }
}

export function* getSignOut(api, action) {
  // const { data } = action;
  yield AsyncStorage.removeItem('Authorization');
  yield put(LoginActions.clear());
  yield put(MeActions.clear());
}
