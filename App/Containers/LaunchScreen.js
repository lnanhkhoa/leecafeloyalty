import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { Video } from 'expo-av';

import { Images, ApplicationStyles, Colors, Fonts } from '../Themes';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
{
  /* <source src="http://leecafeteria.com/wp-content/uploads/2019/11/video-lee-cafe.mp4" type="video/mp4"></source> */
}
const { height, width, fontScale, scale } = Dimensions.get('screen');

const UserItem = props => {
  const { type, name } = props;
  return (
    <View
      style={{
        width: '100%',
        marginVertical: 4,
        paddingVertical: 15,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: Colors.block,
        //
        borderRadius: 5,
      }}>
      <Text>{type}</Text>
      <Text style={{ ...Fonts.size.h4, fontFamily: Fonts.type.bold }}>
        {name}
      </Text>
    </View>
  );
};

function LaunchScreen() {
  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.image}>
          <Image
            style={styles.imageBox}
            source={{
              uri:
                'https://i.dlpng.com/static/png/1322579-child-avatar-icon-flat-design-red-yellow-coffee-avatar-png-693_720_preview.png',
            }}
            // style={{ width: 128, height: 129, borderRadius: 999 }}
          />
        </View>
        <View style={styles.container}>
          <UserItem type="Họ và tên" name="Lê Nguyễn Anh Khoa"/>
          <UserItem type="Username" name="lnanhkhoa"/>
          <UserItem type="Email" name="Lê Nguyễn Anh Khoa"/>
          <UserItem type="Số điện thoại" name="Lê Nguyễn Anh Khoa"/>
        </View>
      </SafeAreaView>
    </View>
  );
}

export default LaunchScreen;

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
    backgroundColor: Colors.white,
  },
  container: {
    paddingHorizontal: 20,
    paddingTop: 10,
    flex: 1,
    // backgroundColor: Colors.block,
  },
  // item
  image: {
    // justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 50,
  },
  imageBox: {
    width: 128,
    height: 128,
    borderRadius: 999,
  },
});
