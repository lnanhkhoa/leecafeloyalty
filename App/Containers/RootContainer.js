import React, { Component, useEffect, useState, memo } from 'react';
import { View, StatusBar, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import StartupActions from '../Redux/StartupRedux';
import ReduxPersist from '../Config/ReduxPersist';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//

import AppNavigation from '../Navigation/AppNavigation';
import AuthNavigation from '../Navigation/AuthNavigation';
import RootAlert from '../Screens/RootAlert';

//
import MeActions, { MeSelectors } from '../Redux/MeRedux';
import NewsActions, { NewsSelectors } from '../Redux/NewsRedux';
import ProductActions, { ProductSelectors } from '../Redux/ProductRedux';
import MachineActions, { MachineSelectors } from '../Redux/MachineRedux';
import LoginActions, { LoginSelectors } from '../Redux/LoginRedux';
const Stack = createStackNavigator();

// Styles
import styles from './Styles/RootContainerStyles';

const _ROUTER = ['auth', 'app'];

function RootContainer(props) {
  const {
    mePayload,
    startup,
    loginPayload,
    getMe,
    getNews,
    getProducts,
    getMachine,
  } = props;

  const [route, setRoute] = useState('auth');
  const [initialScreenAuth, setinitialScreenAuth] = useState('Onboarding');

  // if Authorization existed, getMe
  useEffect(() => {
    if (!ReduxPersist.active) startup();

    if (route === 'auth') {
      AsyncStorage.getItem('Authorization').then(Authorization => {
        if (Authorization) {
          getMe({ Authorization });
        }
      });
    }
    return () => {};
  }, []);

  // if getMe thành công, vô chứ chờ chi
  useEffect(() => {
    if (mePayload) {
      AsyncStorage.getItem('Authorization').then(Authorization => {
        if (Authorization) {
          getNews({
            pageIndex: 0,
            pageSize: 1,
            categoryNo: 1,
          });
          getMachine({ Authorization });
          getProducts({ Authorization });
          setRoute('app');
        }
      });
    }

    if (!mePayload && route !== 'auth') {
      AsyncStorage.getItem('Authorization').then(token => {
        setinitialScreenAuth('Login')
        if (!token) setRoute('auth');
      });
    }

    return () => {};
  }, [mePayload]);

  return (
    <View style={styles.applicationView}>
      <StatusBar barStyle="light-content" />
      <NavigationContainer>
        {route === 'auth' && (
          <AuthNavigation initialRouteName={initialScreenAuth} />
        )}
        {route === 'app' && <AppNavigation />}
      </NavigationContainer>
      <RootAlert />
    </View>
  );
}

const mapStateToProps = state => ({
  mePayload: MeSelectors.getPayload(state.me),
  loginPayload: LoginSelectors.getPayload(state.login),
});

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup()),
  getMe: params => dispatch(MeActions.meRequest({ ...params })),
  getNews: params => dispatch(NewsActions.newsRequest({ ...params })),
  getProducts: params => dispatch(ProductActions.productRequest({ ...params })),
  getMachine: params => dispatch(MachineActions.machineRequest({ ...params })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootContainer);
