import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import {
  ScrollView,
  Text,
  Image,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { Video } from 'expo-av';

import { Images, ApplicationStyles, Colors, Fonts } from '../Themes';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
{
  /* <source src="http://leecafeteria.com/wp-content/uploads/2019/11/video-lee-cafe.mp4" type="video/mp4"></source> */
}
const { height, width, fontScale, scale } = Dimensions.get('screen');

function LaunchScreen() {
  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
          <View style={styles.youtube}>
            <Video
              source={{
                uri:
                  'http://leecafeteria.com/wp-content/uploads/2019/11/video-lee-cafe.mp4',
              }}
              rate={1.0}
              volume={1.0}
              isMuted={false}
              resizeMode="cover"
              shouldPlay
              isLooping
              style={styles.youtube}
            />
          </View>
        <View style={styles.container}>
        </View>
      </SafeAreaView>
    </View>
  );
}

export default LaunchScreen;

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    ...ApplicationStyles.screen.mainContainer,
    backgroundColor: Colors.block,
  },
  container: {
    paddingHorizontal: 20,
    paddingTop: 10,
    flex: 1,
    // backgroundColor: Colors.block,
  },
  // item
  youtube: {
    height: 200,
    width: '100%',
  },
});
