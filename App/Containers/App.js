import DebugConfig from '../Config/DebugConfig';
import React, { Component } from 'react';
import { View } from 'react-native';
import RootContainer from './RootContainer';
import createStore from '../Redux/';
import { Provider } from 'react-redux';

const store = createStore();

function App() {
  return (
    <Provider store={store}>
      <RootContainer />
    </Provider>
  );
}

export default DebugConfig.useReactotron ? console.tron.overlay(App) : App;
