import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  startup: null,
});

export const StartupTypes = Types;
export default Creators;


export const INITIAL_STATE = Immutable({
  startup: null,
});


// request the avatar for a user
export const request = (state, { username }) =>
  state.merge({ fetching: true, username, avatar: null })


export const reducer = createReducer(INITIAL_STATE, {
  [Types.STARTUP]: null
});
