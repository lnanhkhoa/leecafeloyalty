import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
  signUpRequest: ['data'],
  signUpSuccess: ['payload'],
  signUpFailure: ['payload'],
});

export const SignUpTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  errors: null,
});

export const SignUpSelectors = {
  getData: state => state.data,
  getFetching: state => state.fetching,
  getError: state => state.error,
  getPayload: state => state.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = (state, action) => {
  const { payload } = action;
  return state.merge({
    fetching: false,
    errors: payload,
    payload: null,
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGN_UP_REQUEST]: request,
  [Types.SIGN_UP_SUCCESS]: success,
  [Types.SIGN_UP_FAILURE]: failure,
});
