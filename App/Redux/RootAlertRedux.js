import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import _ from 'lodash';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  rootAlertShow: ['errors'],
  rootAlertHide: null,
});

export const RootAlertTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  show: false,
  title: "Thông báo lỗi",
  description: 'Thao tác không thể thực hiện, vui lòng thử lại sau !',
});

/* ------------- Selectors ------------- */

const getShow = state => state.show;
const getErrors = state => state.errors;

export const RootAlertSelectors = {
  selectShow: createSelector([getShow], show => show),
  selectErrors: createSelector([getErrors], errors => errors),
};

/* ------------- Reducers ------------- */

// show
export const show = (state, action) => {
  const { errors } = action;
  return state.merge({ show: true, ...errors });
};

// hide
export const hide = (state, action) => {
  return state.merge({ show: false });
};
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ROOT_ALERT_SHOW]: show,
  [Types.ROOT_ALERT_HIDE]: hide,
});
