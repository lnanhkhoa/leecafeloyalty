import { Dimensions } from 'react-native';
import _ from 'lodash';
const { fontScale } = Dimensions.get('screen');
const type = {
  base: 'OpenSans-Regular',
  bold: 'OpenSans-Bold',
  extraBold: 'OpenSans-ExtraBold',
  light: 'OpenSans-Light',
  italic: 'OpenSans-Italic',
  semiBold: 'OpenSans-SemiBold',
};

function fontScaleFunc(value) {
  return value * fontScale;
}

const _size = {
  h1: 28,
  h2: 26,
  h3: 20,
  h4: 16,
  h5: 14,
  h6: 12,
  input: 18,
  regular: 16,
  medium: 14,
  small: 12,
  tiny: 8.5,
};
const size = _.mapValues(_size, (value, key) => value);

const style = {
  h1: {
    fontFamily: type.extraBold,
    fontSize: size.h1,
  },
  h2: {
    fontFamily: type.bold,
    fontSize: size.h2,
  },
  h3: {
    fontSize: size.h3,
    fontFamily: type.semiBold,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
    letterSpacing: 0.3,
  },
  h6: {
    fontFamily: type.base,
    fontSize: size.h6,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
};

export default {
  type,
  size,
  style,
};
