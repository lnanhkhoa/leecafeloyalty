import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';
import { Dimensions } from 'react-native';

const { height, width, fontScale, scale } = Dimensions.get('screen');
console.log(
  'DebugLog: height, width, fontScale, scale',
  height,
  width,
  width / 375,
  height / 812,
  fontScale,
  scale,
);

const ApplicationStyles = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.background,
    },
    backgroundImage: {
      position: 'absolute',
      height: height,
      width: width,
      // top: 0,
      // left: 0,
      // bottom: 0,
      // right: 0,
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent,
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin,
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center',
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin,
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.text,
    },
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin,
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow,
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center',
  },
  utils: {
    height,
    width,
    fontScale,
    scale,
    resizeWidth(w) {
      return (w / 375) * width;
    },
    resizeHeight(h) {
      return (h / 812) * height;
    },
  },

  // QUAN CREATE //
  specialApp: {
    mainContainer: {
      flex: 1,
    },
    alignCenter: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    hCenter: {
      alignItems: 'center',
    },
    vCenter: {
      justifyContent: 'center',
    },
    itemCenter: {
      alignSelf: 'center',
      alignItems: 'center',
    },

    buttonText: {
      textAlign: 'center',
      fontWeight: 'bold',
      color: Colors.white,
    },
    QrCodeText: {
      fontFamily: Fonts.type.fontBold,
      color: Colors.black,
      fontSize: Fonts.size.textXLarge,
      textAlign: 'center',
    },

    coverBottomTab: {
      position: 'absolute',
      backgroundColor: 'white',
      bottom: 0,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      height: (100 / 375) * Metrics.width,

      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.15,
      shadowRadius: 8,
      // android
      elevation: 30,
    },

    // function
    dimensisons: (width = 0, height = 0) => {
      return {
        width: width === 0 ? undefined : (width / 375) * Metrics.width,
        height: height === 0 ? undefined : (height / 375) * Metrics.width,
      };
    },
    dimensisonsPercentage: (width = '100%', height = '100%') => {
      return { width, height };
    },

    margin: (
      top = 0,
      bottom = 0,
      left = 0,
      right = 0,
      vertical = 0,
      horizontal = 0,
    ) => {
      return {
        marginTop: top === 0 ? undefined : (top / 375) * Metrics.width,
        marginBottom: bottom === 0 ? undefined : (bottom / 375) * Metrics.width,
        marginLeft: left === 0 ? undefined : (left / 375) * Metrics.width,
        marginRight: right === 0 ? undefined : (right / 375) * Metrics.width,
        marginVertical:
          vertical === 0 ? undefined : (vertical / 375) * Metrics.width,
        marginHorizontal:
          horizontal === 0 ? undefined : (horizontal / 375) * Metrics.width,
      };
    },
    border: (width = 0, color = 'white', radius = 0) => {
      return {
        borderWidth: width === 0 ? undefined : width,
        borderColor: color,
        borderRadius: radius === 0 ? undefined : radius,
      };
    },

    padding: (
      top = 0,
      bottom = 0,
      left = 0,
      right = 0,
      vertical = 0,
      horizontal = 0,
    ) => {
      return {
        paddingTop: top === 0 ? undefined : (top / 375) * Metrics.width,
        paddingBottom:
          bottom === 0 ? undefined : (bottom / 375) * Metrics.width,
        paddingLeft: left === 0 ? undefined : (left / 375) * Metrics.width,
        paddingRight: right === 0 ? undefined : (right / 375) * Metrics.width,
        paddingVertical:
          vertical === 0 ? undefined : (vertical / 375) * Metrics.width,
        paddingHorizontal:
          horizontal === 0 ? undefined : (horizontal / 375) * Metrics.width,
      };
    },

    typography: {
      titleAppButton: {
        color: Colors.black,
        fontFamily: Fonts.type.fontRegular,
        fontSize: Fonts.size.textMedium,
      },
    },

    button: {
      buttonBorder: {
        width: (168 / 375) * Metrics.width,
        height: (44 / 375) * Metrics.width,
        borderRadius: Metrics.boderNomarl,
        justifyContent: 'center',
        marginHorizontal: Metrics.marginXTiny,
      },
    },

    shadow: {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.15,
      shadowRadius: 8,
      // android
      elevation: 30,
    },
  },

  swiper: {
    dot: {
      backgroundColor: Colors.steel,
      width: 8,
      height: 8,
      borderRadius: 4,
      marginLeft: 4,
      marginRight: 4,
    },
    dotActive: {
      backgroundColor: Colors.blueOcean,
      width: 8,
      height: 8,
      borderRadius: 4,
      marginLeft: 4,
      marginRight: 4,
    },
  },
};

export default ApplicationStyles;
