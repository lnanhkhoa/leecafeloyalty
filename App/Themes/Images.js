// leave off @2x/@3x
import GiftSvg from '../Images/gift.svg';
import JoystickSvg from '../Images/joystick.svg';
import WalletSvg from '../Images/wallet.svg';
import AddSvg from '../Images/add.svg';
import IllustrationConfirmation from '../Images/IllustrationConfirmation.svg';

const images = {
  JoystickSvg,
  WalletSvg,
  AddSvg,
  GiftSvg,
  IllustrationConfirmation,
  loginBackground: require('../Images/loginbg.jpg'),
  signupBackground: require('../Images/dangky.jpg'),
  loginPerson: require('../Screens/Auth/assets/image-person.png'),
  coffeeBean: require('../Images/coffee-beans.png'),
  coffeelock1: require('../Images/coffeelock1.jpg'),
  coffeelock2: require('../Images/coffeelock2.jpg'),
  coffeelock3: require('../Images/coffeelock3.jpg'),
  coffeelock4: require('../Images/coffeelock4.jpg'),
  tienLoi: require('../Images/tienloi.png'),
  dadangCafe: require('../Images/dadangcafe.jpg'),
  paymentCafe: require('../Images/payment.jpg'),
  logo: require('../../assets/favicon-lee.jpg'),
  developingBG: require('../Images/developing.jpg'),

  pinMarker: require('../Images/pin.png'),
  pinSelectedMarker: require('../Images/pinTapped.png'),
};

export default images;
