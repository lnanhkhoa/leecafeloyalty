const colors = {
  main: '#F7DC4A',
  yellow: '#F7B500',
  linear: {
    start: '#F7B500',
    end: '#f6d365',
  },
  linearButton: {
    start: "#d4fc79",
    end: "#96e6a1"
  },

  bloodOrange: '#FA7268',
  lighterPink: '#EFD9F4',
  lightPink: '#FBC1DA',
  darkPink: '#CE53C7',
  pink: '#B643D5',
  cloud: '#B9B9B9',
  steel: '#DDDDDD',
  textColor: {
    white: '#fff',
    lightTiny: '#D0D0D0',
    tiny: '#727376',
    darkTiny: '#75808F',
    black: '#4E5053',
    color: '#B643D5',
  },

  borderColor: {
    color: '#B643D5',
  },

  background: '#fff',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  silver: '#F7F7F7',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',
  /**
  |--------------------------------------------------
  | Color Theme : ....
  |--------------------------------------------------
  */
  transparent: 'transparent',
  transparent_w: 'rgba(255,255,255,0)',
  transparent_w_50: 'rgba(55,255,255,.1)',
  white: '#FFFFFF',
  black: '#000000',
  red: '#FF5050',
  blue: '#2883E7',
  greenDark: '#00524F',
  greenMedium: '#4BB67D',
  greenLight: '#EDF7F3',
  graySPLight: '#BBBBBB',
  grayLight: '#888888',
  grayMedium: '#444444',
  grayDark: '#222222',
  silver: '#F2F6F9',
  silverDark: '#C7C9D5',
  blueOcean: '#007AFF',
  bar: '#D3E1F1',
  blueheavy: '#0D61FF',
  block: '#EEEEEE',
};

export default colors;
