// a library to wrap and simplify api calls
import apisauce from 'apisauce';

// our "constructor"
const create = (baseURL = 'https://auth.leecafeteria.net/') => {
  const apiAuth = apisauce.create({
    baseURL: 'https://auth.leecafeteria.net/',
    headers: {
      'Cache-Control': 'no-cache',
    },
    timeout: 10000,
  });
  const apiLoyalty = apisauce.create({
    baseURL: 'https://loyalty.leecafeteria.net/',
    headers: {
      'Cache-Control': 'no-cache',
    },
    timeout: 10000,
  });
  const apiPayment = apisauce.create({
    baseURL: 'https://payment.leecafeteria.net/',
    headers: {
      'Cache-Control': 'no-cache',
    },
    timeout: 10000,
  });

  const bearerAuth = Authorization => `Bearer ${Authorization}`;

  // const getRoot = () => apiAuth.get('');
  // const getRate = () => api.get('rate_limit');
  // const getUser = username => api.get('search/users', { q: username });
  const postlogin = data => apiAuth.post('api/User/SignIn', { ...data });
  const postsignUp = data => apiAuth.post('api/User/SignUp', { ...data });
  const getme = ({ Authorization, ...data }) =>
    apiAuth.get(
      'api/User/Me',
      { ...data },
      { headers: { Authorization: bearerAuth(Authorization) } },
    );

  const getnews = data => apiLoyalty.get('api/Post/Paging', { ...data });
  const getproduct = ({ Authorization, ...data }) =>
    apiLoyalty.get(
      'api/Loyalty/Products',
      { ...data },
      { headers: { Authorization: bearerAuth(Authorization) } },
    );
  const getmachine = ({ Authorization, ...data }) =>
    apiLoyalty.get(
      'api/Loyalty/Machines',
      { ...data },
      { headers: { Authorization: bearerAuth(Authorization) } },
    );
  const posttransaction = ({ Authorization, ...data }) =>
    apiLoyalty.post(
      'api/Loyalty/Transactions',
      { ...data },
      { headers: { Authorization: bearerAuth(Authorization) } },
    );
  const postleepayconfirm = ({ Authorization, ...data }) =>
    apiPayment.post(
      'api/LeePay/Confirm',
      { ...data },
      { headers: { Authorization: bearerAuth(Authorization) } },
    );
  return {
    postlogin,
    postsignUp,
    getme,
    getnews,
    getproduct,
    getmachine,
    posttransaction,
    postleepayconfirm,
  };
};

// let's return back our create method as the default.
export default {
  create,
};
